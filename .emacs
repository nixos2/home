;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
;; System-type definition
;; (defun system-is-linux()
;;     (string-equal system-type "gnu/linux"))
;; (defun system-is-windows()
;;     (string-equal system-type "windows-nt"))
;; Start Emacs as a server
;; (when (system-is-linux)
;;     (require 'server)
;;     (or (server-running-p)
;;         (server-start)))
(require 'server)
    (or (server-running-p)
        (server-start))
;; MS Windows path-variable
;;(when (system-is-windows)
;;    (setq win-sbcl-exe          "C:/sbcl/sbcl.exe")
;;    (setq win-init-path         "C:/.emacs.d")
;;    (setq win-init-ct-path      "C:/.emacs.d/plugins/color-theme")
;;    (setq win-init-ac-path      "C:/.emacs.d/plugins/auto-complete")
;;    (setq win-init-slime-path   "C:/slime")
;;    (setq win-init-ac-dict-path "C:/.emacs.d/plugins/auto-complete/dict"))

;; Unix path-variable
;;(when (system-is-linux)
(setq unix-init-path         "~/.emacs.d")
(setq unix-init-ct-path      "~/.emacs.d/plugins/color-theme")
(setq unix-init-ac-path      "~/.emacs.d/plugins/auto-complete")
(setq unix-init-ac-dict-path "~/.emacs.d/plugins/auto-complete/dict")
;;)

;; My name and e-mail adress
(setq user-full-name   "Sergei Shavlovskiy")
(setq user-mail-adress "apterion@yandex.ru")

;;Load files
;; (load-file "~/.emacs.d/init.el")


;; Imenu
(require 'imenu)
(setq imenu-auto-rescan      t)
(setq imenu-use-popup-menu nil)
(global-set-key (kbd "<f6>") 'imenu)
;; Display the name of the current buffer in the title bar
(setq frame-title-format "GNU Emacs: %b")
;;Load path for plugins
;;    (add-to-list 'load-path unix-init-path)
;; Inhibit startup/splash screen
(setq inhibit-splash-screen   t)
(setq inhibit-startup-message t)
;; Show-paren-mode settings
(show-paren-mode t)
(setq show-paren-style 'expression)
;; Electric-modes settings
(electric-pair-mode    1)
(electric-indent-mode  1)
;; Delete selection
(delete-selection-mode t)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Disable GUI components
(tooltip-mode      -1)
(menu-bar-mode     -1)
(tool-bar-mode     -1)
(scroll-bar-mode   -1)
(blink-cursor-mode -1)
(setq use-dialog-box     nil)
(setq redisplay-dont-pause t)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Linum plugin
(require 'linum)
(line-number-mode   t)
(global-linum-mode  t)
(column-number-mode t)
(setq linum-format "%d ")
;; Fringe settings
(fringe-mode '(2 . 0))
(setq-default indicate-empty-lines t)
(setq-default indicate-buffer-boundaries 'left)

;; Display file size/time in mode-line
(setq display-time-24hr-format t)
(display-time-mode             t)
(size-indication-mode          t)
;; IDO plugin
;;(require 'ido)
;;(ido-mode                      t)
;;(icomplete-mode                nil)
;;(ido-everywhere                nil)

;;(setq ido-vitrual-buffers      t)
;;(setq ido-enable-flex-matching t)
;; Buffer Selection and ibuffer settings
(require 'bs)
(require 'ibuffer)
(defalias 'list-buffers 'ibuffer) ;; отдельный список буферов при нажатии C-x C-b
(global-set-key (kbd "C-'") 'bs-show-sorted) ;; запуск buffer selection кнопкой F2
;; Clipboard settings
;;(setq x-select-enable-clipboard t)
;;(setq select-enable-clipboard t)
;; Add melpa
;;(add-to-list 'package-archives
;;  '("melpa" . "http://melpa.milkbox.net/packages/") t)
(require 'package)
(setq package-archives '(("ELPA" . "http://tromey.com/elpa/") 
                         ("gnu" . "http://elpa.gnu.org/packages/")
                         ("marmalade" . "http://marmalade-repo.org/packages/")
                         ("melpa" . "https://melpa.org/packages/")))

;;

;;edts (Erlang)
;;(add-hook 'after-init-hook 'my-after-init-hook)
;;(defun my-after-init-hook ()
;;  (require 'edts-start))
;;
;;Gentoo
;;(add-to-list 'load-path "/usr/lib64/erlang/lib/tools-2.10/emacs")
;;Fedora
;;(add-to-list 'load-path "/usr/lib64/erlang/lib/tools-2.9.1/emacs")
;;(setq load-path (cons  "/usr/lib64/erlang/lib/tools-2.10/emacs"
;;      load-path))
;;      (setq erlang-root-dir "/usr/lib64/erlang")
;;      (setq exec-path (cons "/usr/lib64/erlang/bin" exec-path))
;;      (require 'erlang-start)
;;Tabbar
;;(setq load-path (cons  ".emacs.d/elpa/tabbar-ruler-20160801.2007"
;;      load-path))
;;(setq load-path (cons  ".emacs.d/elpa/tabbar-20160524.1401"
;;      load-path))
;;(setq tabbar-ruler-global-tabbar t)    ; get tabbar
;;(setq tabbar-ruler-global-ruler t)     ; get global ruler
;;(setq tabbar-ruler-popup-menu t)       ; get popup menu.
;;(setq tabbar-ruler-popup-toolbar t)    ; get popup toolbar
;;(setq tabbar-ruler-popup-scrollbar t)  ; show scroll-bar on mouse-move
;;(require 'tabbar-ruler)
;; Disable backup/autosave files
;; Disable autosave
(setq make-backup-files        nil)
(setq auto-save-default        nil)
(setq auto-save-list-file-name nil)
;;
;;Speedbar
(add-to-list 'load-path "~/.emacs.d/elpa/sr-speedbar")
(require 'sr-speedbar)

(add-to-list 'load-path "~/.emacs.d/elpa/yaml-mode")
(require 'yaml-mode)
(add-to-list 'auto-mode-alist '("\\.yml\\'" . yaml-mode))
;;
;;
;;

(package-initialize)
;; (eide-start)
(amx-mode)

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Liberation Sans" :foundry "1ASC" :slant normal :weight normal :height 163 :width normal))))
 '(highlight-indent-guides-character-face ((t (:foreground "#FFFFFF"))))
 '(highlight-indent-guides-odd-face ((t (:background "#FFFFFF"))))
 '(neo-banner-face ((t (:foreground "lightblue" :weight bold))))
 '(neo-button-face ((t (:underline "green"))))
 '(neo-dir-link-face ((t (:foreground "DeepSkyBlue" :height 0.8))))
 '(neo-file-link-face ((t (:distant-foreground "green" :foreground "white" :height 0.8)))))
;; '(tabbar-button ((t (:inherit tabbar-default :box (:line-width 4 :color "white" :style released-button)))))
;; '(tabbar-button-highlight ((t nil)))
;; '(tabbar-default ((t (:background "black" :foreground "white" :height 1.1))))
;; '(tabbar-highlight ((t (:underline (:color foreground-color :style wave)))))
;; '(tabbar-selected ((t (:inherit tabbar-default :foreground "deep sky blue" :box (:line-width 1 :color "white" :style pressed-button)))))
;; '(tabbar-separator ((t nil)))
;; '(tabbar-unselected ((t (:box (:line-width 1 :color "white" :style released-button))))))

(global-set-key (kbd "C-x C-b") 'ibuffer)
(autoload 'ibuffer "ibuffer" "List buffers." t)

(windmove-default-keybindings 'meta)
(fset 'yes-or-no-p 'y-or-n-p)

;;Save sessions
;; EDTS no work with it. TODO
(desktop-save-mode t)

;;eMail
;;(autoload 'wl "wl" "Wanderlust" t)
;;(add-to-list 'load-path "~/.emacs.d/email.el")

;; Indent guides
(setq highlight-indent-guides-method 'character)
;;(setq highlight-indent-guides-method 'column)
;;(setq highlight-indent-guides-method 'fill)
;;(setq highlight-indent-guides-character ?\|)
(setq highlight-indent-guides-auto-odd-face-perc 15)
(setq highlight-indent-guides-auto-even-face-perc 15)
(setq highlight-indent-guides-auto-character-face-perc 20)
;;(setq highlight-indent-guides-auto-enabled nil)

;;(set-face-background 'highlight-indent-guides-odd-face "darkgray")
;;(set-face-background 'highlight-indent-guides-even-face "dimgray")
;;(set-face-foreground 'highlight-indent-guides-character-face "dimgray")

(add-to-list 'load-path "~/.emacs.d/elpa/highligt-indent-guides")
(add-hook 'prog-mode-hook 'highlight-indent-guides-mode)
;;(setq highlight-indent-quides-method 'character)

;; Yet another indent
(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)
(setq indent-line-function 'insert-tab)

;;(setq highlight-indent-quides-method 'column)

;;Buffer manage
(add-to-list 'load-path "~/.emacs.d/elpa/buffer-manage")
(require 'buffer-manage)

;;Magit
(add-to-list 'load-path "~/.emacs.d/elpa/magit")
(add-to-list 'load-path "~/.emacs.d/elpa/magit-popup")
(require 'magit)
(require 'magit-popup)
;;magit-gitflow
(setq magit-gitflow-popup-key "M-f")
(require 'magit-gitflow)
(add-hook 'magit-mode-hook 'turn-on-magit-gitflow)
;; magit-todo
;; Not work
;;(add-to-list 'load-path "~/.emacs.d/elpa/magit-todos")
;;(require magit-todos)
;;(use-package magit-todos
;;  :load-path "~/CodeWorks/emacs/magit-todos"
;;  :commands (magit-todos-mode)
;;  :hook (magit-mode . magit-todos-mode)
;;  :config
;;  (setq magit-todos-recursive t
;;        magit-todos-depth 100)
;;  :custom (magit-todos-keywords (list "TODO" "FIXME" "NEXT")))

;; Egg (clone of Magit)
(add-to-list 'load-path "~/.emacs.d/elpa/egg")
(require 'egg)

(setq ediff-split-window-function 'split-window-horizontally
      ediff-window-setup-function 'ediff-setup-windows-plain)
(setq ediff-diff-options "-w") ;; ignore whitespace

;; Magit-vdiff (vim style diff)
;; (add-to-list 'load-path "~/.emacs.d/elpa/vdiff")
;; (require 'vdiff)
;; (define-key vdiff-mode-map (kbd "C-,") vdiff-mode-prefix-map)
;; (add-to-list 'load-path "~/.emacs.d/elpa/vdiff-magit")
;; (require 'vdiff-magit)
;; (define-key magit-mode-map "e" 'vdiff-magit-dwim)
;; (define-key magit-mode-map "E" 'vdiff-magit-popup)
;; (setcdr (assoc ?e (plist-get magit-dispatch-popup :actions))
;;         '("vdiff dwim" 'vdiff-magit-dwim))
;; (setcdr (assoc ?E (plist-get magit-dispatch-popup :actions))
;;         '("vdiff popup" 'vdiff-magit-popup))

;;Char-menu
(add-to-list 'load-path "~/.emacs.d/elpa/char-menu")
(require 'char-menu)

;;JS

(add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))
(add-to-list 'load-path "~/.emacs.d/elpa/js2-mode")
(require 'js2-mode)

(eval-after-load 'js2-mode
  '(add-hook 'js2-mode-hook (lambda () (add-hook 'after-save-hook 'eslint-fix nil t))))

(add-to-list 'load-path "~/.emacs.d/elpa/ac-js2")
(require 'ac-js2)
(add-to-list 'load-path "~/.emacs.d/elpa/tern")
(require 'tern)
(add-to-list 'load-path "~/.emacs.d/elpa/tern-auto-complete")
(require 'tern-auto-complete)
(auto-complete-mode)
(autoload 'tern-mode "tern.el" nil t)
(add-hook 'js-mode-hook (lambda () (tern-mode t)))
(add-hook 'js-mode-hook (lambda () (auto-complete-mode)))
(eval-after-load 'tern
   '(progn
      (require 'tern-auto-complete)
     (auto-complete-mode)
      (tern-ac-setup)))

;;(load-file "~/.emacs.d/player.el")
;;(load-file "~/.emacs.d/tabs.el")

;; TODO, FIXME etc (should be loaded before hotkeys.el)
(add-to-list 'load-path "~/.emacs.d/elpa/hl-todo")
(require 'hl-todo)
(global-hl-todo-mode t)

(load-file "~/.emacs.d/hotkeys.el")

;; projectile
(add-to-list 'load-path "~/.emacs.s/elpa/projectile")
(require 'projectile)
;;(setq projectile-project-search-path '("~/develop"))
(projectile-mode +1)
(define-key projectile-mode-map (kbd "s-p") 'projectile-command-map)
(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)

(add-to-list 'load-path "~/.emacs.d/elpa/helm")
(require 'helm)
(add-to-list 'load-path "~/.emacs.d/elpa/helm-projectile")
(require 'helm-projectile)

;; snippets

;;(add-to-list 'load-path "~/.emacs.d/elpa/yasnippet")
;;(require 'yasnippet)
;;(yas-global-mode 1)

;;(load-file "~/.emacs.d/custom/snippets.el")

;; Start up with tasks.org
;; (find-file "~/Dropbox/org/Firelink web.org")

;; PDF
;;(add-to-list 'load-path "~/.emacs.d/elpa/pdf-tools")
;;(require 'pdf-tools)
;;(add-to-list 'load-path "~/.emacs.d/elpa/org-pdfview")
;;(require 'org-pdfview)
;;(add-to-list 'org-file-apps '("\\.pdf\\'" . (lambda (file link) (org-pdfview-open link))))

;; Angular
;;(add-to-list 'load-path "~/.emacs.d/elpa/ng2-mode")
;;(require 'ng2-mode)

;; Vue

(add-to-list 'load-path "~/.emacs.d/elpa/vue-mode")
(require 'vue-mode)

(defun vue-mode/init-vue-mode ()
  (use-package vue-mode
               :config
               ;; 0, 1, or 2, representing (respectively) none, low, and high coloring
               (setq mmm-submode-decoration-level 2)
))

;; Handlebars
;;(add-to-list 'load-path "~/.emacs.d/elpa/handlebars-mode")
;;(require 'handlebars-mode)

;; HTML
;;(add-to-list 'load-path "~/.emacs.d/elpa/ac-html")
;;(require 'ac-html)
(add-to-list 'load-path "~/.emacs.d/elpa/web-mode")
(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.html$" . web-mode))
(add-to-list 'auto-mode-alist '("\\.vue$" . web-mode))
;;(add-to-list 'auto-mode-alist '("\\.yaws$" . web-mode))

;; flow-minor-mode
;;(add-hook 'js2-mode-hook 'flow-minor-enable-automatically)
;;(add-hook 'js2-mode-hook 'flow-minor-mode)

;; TRAMP
(setq tramp-default-method "ssh")
(setq tramp-shell-prompt-pattern "^[^$>\n]*[#$%>] *\\(\[[0-9;]*[a-zA-Z] *\\)*")
;; (setq tramp-shell-prompt-pattern ".*")
;;;;;;

;; Python
;;(add-to-list 'load-path "~/.emacs.d/elpa/ac-python")
;;(require 'ac-python)
;;(load "~/.emacs.d/elpa/ac-python/ac-python.el")

;;Erlang path problems
;;(add-to-list 'load-path "~/.emacs.d/elpa/projectile")
;;(require 'projectile)


;;(if window-system
      
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(amx-mode t)
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#212526" "#ff4b4b" "#b4fa70" "#fce94f" "#729fcf" "#e090d7" "#8cc4ff" "#eeeeec"])
 '(ansi-term-color-vector
   [unspecified "#1F1611" "#660000" "#144212" "#EFC232" "#5798AE" "#BE73FD" "#93C1BC" "#E6E1DC"] t)
 '(beacon-color "#ed0547ad8099")
 '(blink-cursor-mode nil)
 '(bongo-enabled-backends (quote (vlc)))
 '(column-number-mode t)
 '(company-quickhelp-color-background "#b0b0b0")
 '(company-quickhelp-color-foreground "#232333")
 '(compilation-message-face (quote default))
 '(cua-global-mark-cursor-color "#7ec98f")
 '(cua-normal-cursor-color "#8c8b85")
 '(cua-overwrite-cursor-color "#e5c06d")
 '(cua-read-only-cursor-color "#8ac6f2")
 '(cursor-type (quote bar))
 '(custom-enabled-themes (quote (gruvbox-black-hard)))
 '(custom-safe-themes
   (quote
    ("eabbb4b4d2430bd2f0fccee36fbc39bf289ee83bfda55074ed787af4595d7bf1" "16953ec5995f7bcf99afe7b8b62c4404e897a417aea1205387e25257fdc2bfbe" "da3d7942b5845592cdf106d754736a237255258430475e26e661fd57cbb8b4ed" "b02c9ba688fc09763ccc66fea21bfe8578f9086beabc926961f77e1225060eb7" "1bb3bb74c8f51d2db6e7cbbf13a68f6777685be42e9e3847c611fa0709b1c0ab" "921636d85a15b570024ef92e89665538f99e25fd8ad6aa770269ef188a6ae913" "a06658a45f043cd95549d6845454ad1c1d6e24a99271676ae56157619952394a" "939ea070fb0141cd035608b2baabc4bd50d8ecc86af8528df9d41f4d83664c6a" "aded61687237d1dff6325edb492bde536f40b048eab7246c61d5c6643c696b7f" "4cf9ed30ea575fb0ca3cff6ef34b1b87192965245776afa9e9e20c17d115f3fb" "3882f886c7b362564bc39cfaa9af26efc00c11f17f4a89515bd61a8d0b1395a0" "d948a009841902d7265b5e8257455cccfd25d250502cdab3b9ff5cd771409a07" "30289fa8d502f71a392f40a0941a83842152a68c54ad69e0638ef52f04777a4c" "08ed410f71bd79e1f7dfb03d7defa478047a47149ff7b0a93c854b1ff55e9807" "14a4bbd2207617728ea504ea9aa48416999a456db9f10e7d74baab896301d8a3" "e3fe955862c63980e314e670a0e74f20825eaed867710ba32be6d6f261e40f9f" "36f17556e827b41b63fe9375dbeeb4989d4976fe51cd0506968c6a41d2a7c9f8" "57290e991bf30a375509b74ea7ecfdb5de5150e0a14130c925582726f003c919" "37ba833442e0c5155a46df21446cadbe623440ccb6bbd61382eb869a2b9e9bf9" "880f541eabc8c272d88e6a1d8917fe743552f17cedd8f138fe85987ee036ad08" "76935a29af65f8c915b1b3b4f6326e2e8d514ca098bd7db65b0caa533979fc01" "62a6731c3400093b092b3837cff1cb7d727a7f53059133f42fcc57846cfa0350" "0f302165235625ca5a827ac2f963c102a635f27879637d9021c04d845a32c568" "2047464bf6781156ebdac9e38a17b97bd2594b39cfeaab561afffcbbe19314e2" "aae40caa1c4f1662f7cae1ebfbcbb5aa8cf53558c81f5bc15baefaa2d8da0241" "aaf783d4bfae32af3e87102c456fba8a85b79f6e586f9911795ea79055dee3bf" "9d9b2cf2ced850aad6eda58e247cf66da2912e0722302aaa4894274e0ea9f894" "ec0c9d1715065a594af90e19e596e737c7b2cdaa18eb1b71baf7ef696adbefb0" "5c5de678730ceb4e05794431dd65f30ffe9f1ed6c016fa766cdf909ba03e4df4" "995d0754b79c4940d82bd430d7ebecca701a08631ec46ddcd2c9557059758d33" "70b2d5330a8dd506accac4b51aaa7e43039503d000852d7d152aec2ce779d96d" "011d4421eedbf1a871d1a1b3a4d61f4d0a2be516d4c94e111dfbdc121da0b043" "6b4f7bde1ce64ea4604819fe56ff12cda2a8c803703b677fdfdb603e8b1f8bcb" "31772cd378fd8267d6427cec2d02d599eee14a1b60e9b2b894dd5487bd30978e" "a11043406c7c4233bfd66498e83600f4109c83420714a2bd0cd131f81cbbacea" "780c67d3b58b524aa485a146ad9e837051918b722fd32fd1b7e50ec36d413e70" "b4fd44f653c69fb95d3f34f071b223ae705bb691fb9abaf2ffca3351e92aa374" "9dc64d345811d74b5cd0dac92e5717e1016573417b23811b2c37bb985da41da2" "5eb4b22e97ddb2db9ecce7d983fa45eb8367447f151c7e1b033af27820f43760" "77515a438dc348e9d32310c070bfdddc5605efc83671a159b223e89044e4c4f1" "2d5c40e709543f156d3dee750cd9ac580a20a371f1b1e1e3ecbef2b895cf0cd2" "a455366c5cdacebd8adaa99d50e37430b0170326e7640a688e9d9ad406e2edfd" "ab98c7f7a58add58293ac67bec05ae163b5d3f35cddf18753b2b073c3fcd8841" "e677cc0546b0c129fda0675354245513543a56671d9747c81d335505f699000b" "43b219a31db8fddfdc8fdbfdbd97e3d64c09c1c9fdd5dff83f3ffc2ddb8f0ba0" "4b19d61c560a93ef90767abe513c11f236caec2864617d718aa366618133704c" "4c0739c6ad6fd91ebd737f8f40527d279cc5f85bc286a7c0d7467b4a6ba53166" "5d75f9080a171ccf5508ce033e31dbf5cc8aa19292a7e0ce8071f024c6bcad2a" "bd82c92996136fdacbb4ae672785506b8d1d1d511df90a502674a51808ecc89f" "669e02142a56f63861288cc585bee81643ded48a19e36bfdf02b66d745bcc626" "44bff5692b73759fa076b42ce37547d2ae98499d5c700bc8297973cb81212dcf")))
 '(diary-entry-marker (quote font-lock-variable-name-face))
 '(display-time-mode t)
 '(ediff-prefer-iconified-control-frame t)
 '(ediff-window-setup-function (quote ediff-setup-windows-plain))
 '(egg-enable-tooltip t)
 '(emms-mode-line-icon-image-cache
   (quote
    (image :type xpm :ascent center :data "/* XPM */
static char *note[] = {
/* width height num_colors chars_per_pixel */
\"    10   11        2            1\",
/* colors */
\". c #1fb3b3\",
\"# c None s None\",
/* pixels */
\"###...####\",
\"###.#...##\",
\"###.###...\",
\"###.#####.\",
\"###.#####.\",
\"#...#####.\",
\"....#####.\",
\"#..######.\",
\"#######...\",
\"######....\",
\"#######..#\" };")))
 '(ensime-sem-high-faces
   (quote
    ((var :foreground "#9876aa" :underline
          (:style wave :color "yellow"))
     (val :foreground "#9876aa")
     (varField :slant italic)
     (valField :foreground "#9876aa" :slant italic)
     (functionCall :foreground "#a9b7c6")
     (implicitConversion :underline
                         (:color "#808080"))
     (implicitParams :underline
                     (:color "#808080"))
     (operator :foreground "#cc7832")
     (param :foreground "#a9b7c6")
     (class :foreground "#4e807d")
     (trait :foreground "#4e807d" :slant italic)
     (object :foreground "#6897bb" :slant italic)
     (package :foreground "#cc7832")
     (deprecated :strike-through "#a9b7c6"))))
 '(eslintd-fix-executable "/home/apterion/.yarn/bin/eslint_d")
 '(evil-emacs-state-cursor (quote ("#E57373" hbar)))
 '(evil-insert-state-cursor (quote ("#E57373" bar)))
 '(evil-normal-state-cursor (quote ("#FFEE58" box)))
 '(evil-visual-state-cursor (quote ("#C5E1A5" box)))
 '(fci-rule-character-color "#452E2E")
 '(fci-rule-color "#3E4451")
 '(flymake-error-bitmap
   (quote
    (flymake-double-exclamation-mark modus-theme-fringe-red)))
 '(flymake-note-bitmap (quote (exclamation-mark modus-theme-fringe-cyan)))
 '(flymake-warning-bitmap (quote (exclamation-mark modus-theme-fringe-yellow)))
 '(frame-background-mode (quote dark))
 '(frame-brackground-mode (quote dark))
 '(frame-resize-pixelwise t)
 '(fringe-mode 6 nil (fringe))
 '(gnus-logo-colors (quote ("#528d8d" "#c0c0c0")) t)
 '(gnus-mode-line-image-cache
   (quote
    (image :type xpm :ascent center :data "/* XPM */
static char *gnus-pointer[] = {
/* width height num_colors chars_per_pixel */
\"    18    13        2            1\",
/* colors */
\". c #1fb3b3\",
\"# c None s None\",
/* pixels */
\"##################\",
\"######..##..######\",
\"#####........#####\",
\"#.##.##..##...####\",
\"#...####.###...##.\",
\"#..###.######.....\",
\"#####.########...#\",
\"###########.######\",
\"####.###.#..######\",
\"######..###.######\",
\"###....####.######\",
\"###..######.######\",
\"###########.######\" };")) t)
 '(handlebars-basic-offset 4)
 '(helm-autoresize-mode t)
 '(highlight-changes-colors (quote ("#ff8eff" "#ab7eff")))
 '(highlight-indent-guides-auto-enabled nil)
 '(highlight-symbol-colors
   (quote
    ("#FFEE58" "#C5E1A5" "#80DEEA" "#64B5F6" "#E1BEE7" "#FFCC80")))
 '(highlight-symbol-foreground-color "#E0E0E0")
 '(highlight-tail-colors (quote (("#ed0547ad8099" . 0) ("#424242" . 100))))
 '(history-mode t)
 '(history-window-local-history t)
 '(hl-bg-colors
   (quote
    ("#4c4436" "#4a4136" "#4f4340" "#4c3935" "#3b313d" "#40424a" "#3a463b" "#3d454c")))
 '(hl-fg-colors
   (quote
    ("#2a2929" "#2a2929" "#2a2929" "#2a2929" "#2a2929" "#2a2929" "#2a2929" "#2a2929")))
 '(hl-paren-background-colors (quote ("#e8fce8" "#c1e7f8" "#f8e8e8")))
 '(hl-paren-colors (quote ("#7ec98f" "#e5c06d" "#a4b5e6" "#834c98" "#8ac6f2")))
 '(hl-sexp-background-color "#efebe9")
 '(hl-todo-keyword-faces
   (quote
    (("HOLD" . "#d0bf8f")
     ("TODO" . "#cc9393")
     ("NEXT" . "#dca3a3")
     ("THEM" . "#dc8cc3")
     ("PROG" . "#7cb8bb")
     ("OKAY" . "#7cb8bb")
     ("DONT" . "#5f7f5f")
     ("FAIL" . "#8c5353")
     ("DONE" . "#afd8af")
     ("NOTE" . "#d0bf8f")
     ("KLUDGE" . "#d0bf8f")
     ("HACK" . "#d0bf8f")
     ("TEMP" . "#d0bf8f")
     ("FIXME" . "#cc9393")
     ("XXX" . "#cc9393")
     ("XXXX" . "#cc9393")
     ("todo" . "#cc9393")
     ("fixme" . "#cc9393")
     ("@todo" . "#cc9393")
     ("@fixme" . "#cc9393"))))
 '(ibuffer-deletion-face (quote dired-flagged))
 '(ibuffer-filter-group-name-face (quote dired-mark))
 '(ibuffer-marked-face (quote dired-marked))
 '(ibuffer-title-face (quote dired-header))
 '(ibuffer-use-other-window nil)
 '(ibuffer-view-ibuffer nil)
 '(indicate-buffer-boundaries nil)
 '(js-flat-functions t)
 '(js2-mode-assume-strict t)
 '(line-spacing 0.2)
 '(linum-format (quote dynamic))
 '(lsp-ui-doc-border "#999791")
 '(magit-diff-use-overlays nil)
 '(main-line-color1 "#222912")
 '(main-line-color2 "#09150F")
 '(mmm-global-mode (quote maybe) nil (mmm-mode))
 '(mmm-parse-when-idle nil)
 '(neo-auto-indent-point t)
 '(neo-autorefresh nil)
 '(neo-smart-open t)
 '(neo-theme (quote classic))
 '(neo-window-width 35)
 '(nrepl-message-colors
   (quote
    ("#ffb4ac" "#ddaa6f" "#e5c06d" "#3d454c" "#e2e9ea" "#40424a" "#7ec98f" "#e5786d" "#834c98")))
 '(org-agenda-files (quote ("~/Dropbox/org/Firelink web.org")))
 '(org-todo-keywords (quote ((sequence "TODO" "DONE" "TEST"))))
 '(package-selected-packages
   (quote
    (yasnippet exec-path-from-shell gruvbox-theme purescript-mode typescript-mode ghc company company-tabnine flymake-haskell-multi tmux-pane hindent fish-mode flymake-hlint hasky-stack haskell-mode docker eide amx telega stylus-mode elixir-mode realgud-node-inspect flymake-eslint nginx-mode magit-todos magit-reviewboard magit-org-todos magit-gitflow flow-minor-mode multi-web-mode eslintd-fix lsp-vue iss-mode js3-mode js2-mode pug-mode egg vdiff-magit go-guru go-direx go-scratch gotest flycheck multi-compile go-rename company-go go-eldoc go-mode dumb-jump helm-projectile docker-tramp counsel-tramp hl-todo fic-mode js-doc neotree history jade-mode less-css-mode projectile handlebars-mode yaml-mode vue-mode buffer-manage bs-ext company-tern web-mode ac-html vue-html-mode ng2-mode window-purpose tern-auto-complete sr-speedbar org-pdfview minimap magit highlight-indent-guides eslint-fix char-menu bongo auto-complete-distel atom-one-dark-theme ac-js2 ac-etags)))
 '(pdf-view-midnight-colors (quote ("#FDF4C1" . "#282828")))
 '(pos-tip-background-color "#3a933a933a93")
 '(pos-tip-foreground-color "#9E9E9E")
 '(powerline-color1 "#222912")
 '(powerline-color2 "#09150F")
 '(projectile-globally-ignored-directories
   (quote
    (".idea" ".ensime_cache" ".eunit" ".git" ".hg" ".fslckout" "_FOSSIL_" ".bzr" "_darcs" ".tox" ".svn" ".stack-work" "node_modules" "dist")))
 '(projectile-use-git-grep t)
 '(projectile-verbose nil)
 '(red "#ffffff")
 '(safe-local-variable-values
   (quote
    ((haskell-process-use-ghci . t)
     (haskell-indent-spaces . 4))))
 '(show-paren-mode t)
 '(size-indication-mode t)
 '(smartrep-mode-line-active-bg (solarized-color-blend "#8ac6f2" "#2f2f2d" 0.2))
 '(sml/active-background-color "#98ece8")
 '(sml/active-foreground-color "#424242")
 '(sml/inactive-background-color "#4fa8a8")
 '(sml/inactive-foreground-color "#424242")
 '(speedbar-show-unknown-files t)
 '(speedbar-supported-extension-expressions
   (quote
    (".org" ".[ch]\\(\\+\\+\\|pp\\|c\\|h\\|xx\\)?" ".tex\\(i\\(nfo\\)?\\)?" ".el" ".emacs" ".l" ".lsp" ".p" ".java" ".js" ".f\\(90\\|77\\|or\\)?" ".ad[abs]" ".p[lm]" ".tcl" ".m" ".scm" ".pm" ".py" ".g" ".s?html" ".ma?k" "[Mm]akefile\\(\\.in\\)?" ".erl" ".hrl" ".conf" ".cfg" ".vue")))
 '(sr-speedbar-auto-refresh t)
 '(standard-indent 2)
 '(tabbar-background-color "#357535753575")
 '(term-default-bg-color "#2a2929")
 '(term-default-fg-color "#8c8b85")
 '(tetris-x-colors
   [[229 192 123]
    [97 175 239]
    [209 154 102]
    [224 108 117]
    [152 195 121]
    [198 120 221]
    [86 182 194]])
 '(tool-bar-mode nil)
 '(vc-annotate-background nil)
 '(vc-annotate-background-mode nil)
 '(vc-annotate-color-map
   (quote
    ((20 . "#d54e53")
     (40 . "goldenrod")
     (60 . "#e7c547")
     (80 . "DarkOliveGreen3")
     (100 . "#70c0b1")
     (120 . "DeepSkyBlue1")
     (140 . "#c397d8")
     (160 . "#d54e53")
     (180 . "goldenrod")
     (200 . "#e7c547")
     (220 . "DarkOliveGreen3")
     (240 . "#70c0b1")
     (260 . "DeepSkyBlue1")
     (280 . "#c397d8")
     (300 . "#d54e53")
     (320 . "goldenrod")
     (340 . "#e7c547")
     (360 . "DarkOliveGreen3"))))
 '(vc-annotate-very-old-color nil)
 '(vue-modes
   (quote
    ((:type template :name nil :mode vue-html-mode)
     (:type template :name html :mode vue-html-mode)
     (:type template :name jade :mode jade-mode)
     (:type template :name pug :mode pug-mode)
     (:type template :name slm :mode slim-mode)
     (:type template :name slim :mode slim-mode)
     (:type script :name nil :mode js2-mode)
     (:type script :name js :mode js2-mode)
     (:type script :name es6 :mode js2-mode)
     (:type script :name babel :mode js2-mode)
     (:type script :name coffee :mode coffee-mode)
     (:type script :name ts :mode js2-mode)
     (:type script :name typescript :mode js2-mode)
     (:type style :name nil :mode css-mode)
     (:type style :name css :mode css-mode)
     (:type style :name stylus :mode stylus-mode)
     (:type style :name less :mode less-css-mode)
     (:type style :name scss :mode css-mode)
     (:type style :name sass :mode ssass-mode)
     (:type style :name styl :mode stylus-mode))))
 '(web-mode-code-indent-offset 2)
 '(web-mode-css-indent-offset 2)
 '(web-mode-enable-comment-interpolation t)
 '(web-mode-enable-current-column-highlight t)
 '(web-mode-enable-element-content-fontification t)
 '(web-mode-enable-engine-detection t)
 '(web-mode-enable-optional-tags t)
 '(web-mode-enable-part-face t)
 '(web-mode-markup-indent-offset 2)
 '(web-mode-part-padding 0)
 '(web-mode-script-padding 0)
 '(web-mode-script-template-types
   (quote
    ("text/x-handlebars" "text/x-jquery-tmpl" "text/x-jsrender" "text/html" "text/ng-template" "text/x-template" "text/mustache" "text/x-dust-template" "text/x-pug")))
 '(web-mode-sql-indent-offset 0)
 '(web-mode-style-padding 0)
 '(weechat-color-list
   (unspecified "#242728" "#323342" "#F70057" "#ff0066" "#86C30D" "#63de5d" "#BEB244" "#E6DB74" "#40CAE4" "#06d8ff" "#FF61FF" "#ff8eff" "#00b2ac" "#53f2dc" "#f8fbfc" "#ffffff"))
 '(when
      (or
       (not
        (boundp
         (quote ansi-term-color-vector)))
       (not
        (facep
         (aref ansi-term-color-vector 0)))))
 '(window-divider-default-bottom-width 1)
 '(window-divider-default-right-width 1)
 '(window-divider-mode t)
 '(xterm-color-names
   ["#2f2f2d" "#ffb4ac" "#8ac6f2" "#e5c06d" "#a4b5e6" "#e5786d" "#7ec98f" "#e8e5db"])
 '(xterm-color-names-bright
   ["#2a2929" "#ddaa6f" "#6a6965" "#74736e" "#8c8b85" "#834c98" "#999791" "#f6f2e8"])
 '(yaml-indent-offset 4))

  ;;(set-foreground-color "#66aaff")
;;  (set-foreground-color "#ffffff")
  ;; (text-scale-adjust -2)
(set-background-color "#050505")
(set-face-attribute 'default nil :height 190)
;;(set-frame-parameter (selected-frame) 'alpha '(<active> . <inactive>))
 ;;(set-frame-parameter (selected-frame) 'alpha <both>)

;; Transparency
;; (set-frame-parameter (selected-frame) 'alpha '(90 . 50))
;; (add-to-list 'default-frame-alist '(alpha . (90 . 50)))

;; Korean
;;(setq default-korean-keyboard "1")
;;(setq default-input-method "korean-hangul")


;;Vue lsp
;; (add-to-list 'load-path "~/.emacs.d/elpa/lsp-mode")
;; (add-to-list 'load-path "~/.emacs.d/elpa/lsp-vue")
;; (require 'lsp-mode)
;; (require 'lsp-vue)

;; Haskell
;; Hlint
(add-hook 'haskell-mode-hook #'flymake-hlint-load)

;; Tabnine

(add-hook 'haskell-mode-hook (lambda ()
                          (set (make-local-variable 'company-backends) '(company-tabnine))
                          (company-mode)))

(add-hook 'typescript-mode-hook (lambda ()
                          (set (make-local-variable 'company-backends) '(company-tabnine))
                          (company-mode)))


;; Structured Haskell Mode
;; (add-to-list 'load-path "~/.emacs.d/shm")
;; (require 'shm)
;; (setq shm-program-name "~/.emacs.d/shm/structured-haskell-mode")
;; (add-hook 'haskell-mode-hook 'structured-haskell-mode)

;; ESLint
(add-to-list 'load-path "~/.emacs.d/elpa/eslintd-fix")
(require 'eslintd-fix)
(add-hook 'js-mode-hook #'eslintd-fix-mode)
(add-hook 'vue-mode-hook #'eslintd-fix-mode)
(add-hook 'js2-mode-hook #'eslintd-fix-mode)

;; Realgud
;; node-inspect
(add-hook 'realgud-short-key-mode-hook
        (lambda ()
          (local-set-key "\C-c" realgud:shortkey-mode-map)))

;;Less mode
(add-to-list 'load-path "~/.emacs.d/elpa/less-css-mode")
(require 'less-css-mode)

;;Jade mode
;;(add-to-list 'load-path "~/.emacs.d/elpa/less-css-mode")
;;(require 'less-css-mode)

;; Minibuffer history
(savehist-mode 1)
;; File history
;;(history-add-history t)

;; Project tree
(add-to-list 'load-path "~/.emacs.d/elpa/neotree")
  (require 'neotree)
  (global-set-key [f8] 'neotree-toggle)

;; JS-Doc
(add-to-list 'load-path "~/.emacs.d/elpa/js-doc")
(require 'js-doc)
(setq js-doc-mail-address "apterion@yandex.ru"
      js-doc-author (format "Sergei Shavlovskiy <%s>" js-doc-mail-address)
      ;;js-doc-url ""
      ;; js-doc-license "license name")
)
(add-hook 'js2-mode-hook
          #'(lambda ()
              (define-key js2-mode-map "\C-ci" 'js-doc-insert-function-doc)
              (define-key js2-mode-map "@" 'js-doc-insert-tag)))


;; Special buffer
(setq special-display-buffer-names 
      '(("magic buffer"  (width . 70)
              (height . 7)
              (background-color . "green"))
       ))

;; Docker, vagrant etc
;;(exec-path-from-shell-initialize)
(add-to-list 'load-path "~/.emacs.d/elpa/counsel")
(require 'counsel)
(add-to-list 'load-path "~/.emacs.d/elpa/counsel-tramp")
(require 'counsel-tramp)
(add-to-list 'load-path "~/.emacs.d/elpa/docker-tramp")
(require 'docker-tramp)
;;(add-to-list 'load-path "~/.emacs.d/elpa/vagrant-tramp")
;;(require 'vagrant-tramp)
;; For speed up tramp
(add-hook 'counsel-tramp-pre-command-hook '(lambda () (global-aggressive-indent-mode 0)
				     (projectile-mode 0)
				     (editorconfig-mode 0)))
(add-hook 'counsel-tramp-quit-hook '(lambda () (global-aggressive-indent-mode 1)
			      (projectile-mode 1)
			      (editorconfig-mode 1)))

;; Jump to definition
(add-to-list 'load-path "~/.emacs.d/elpa/dumb-jump")
(require 'dumb-jump)
(dumb-jump-mode)


;; Go

(require 'company)
(require 'flycheck)
(require 'yasnippet)
(require 'multi-compile)
(require 'go-eldoc)
(require 'company-go)

(add-hook 'before-save-hook 'gofmt-before-save)
(setq-default gofmt-command "goimports")
(add-hook 'go-mode-hook 'go-eldoc-setup)
(add-hook 'go-mode-hook (lambda ()
                            (set (make-local-variable 'company-backends) '(company-go))
                            (company-mode)))
(add-hook 'go-mode-hook 'yas-minor-mode)
(add-hook 'go-mode-hook 'flycheck-mode)
(setq multi-compile-alist '(
    (go-mode . (
("go-build" "go build -v"
   (locate-dominating-file buffer-file-name ".git"))
("go-build-and-run" "go build -v && echo 'build finish' && eval ./${PWD##*/}"
   (multi-compile-locate-file-dir ".git"))))
    ))

;;(add-to-list 'load-path "~/.emacs.d/elpa/go-mode")
;;(require 'go-mode)
;;(add-to-list 'load-path "~/.emacs.d/elpa/company")
;;(require 'company)
;;(add-to-list 'load-path "~/.emacs.d/elpa/flycheck")
;;(require 'flycheck)
;;(add-to-list 'load-path "~/.emacs.d/elpa/multi-compile")
;;(require 'multi-compile)
;;(add-to-list 'load-path "~/.emacs.d/elpa/go-eldoc")
;;(require 'go-eldoc)
;;(add-to-list 'load-path "~/.emacs.d/elpa/company-go")
;;(require 'company-go)

(add-hook 'before-save-hook 'gofmt-before-save)
(setq-default gofmt-command "goimports")
(add-hook 'go-mode-hook 'go-eldoc-setup)
(add-hook 'go-mode-hook (lambda ()
                          (set (make-local-variable 'company-backends) '(company-go))
                          (company-mode)))
(add-hook 'go-mode-hook 'yas-minor-mode)
(add-hook 'go-mode-hook 'flycheck-mode)
(setq multi-compile-alist '(
                            (go-mode . (
                                        ("go-build" "go build -v"
                                         (locate-dominating-file buffer-file-name ".git"))
                                        ("go-build-and-run" "go build -v && echo 'build finish' && eval ./${PWD##*/}"
                                         (multi-compile-locate-file-dir ".git"))))
                            ))
(require 'nginx-mode)
(exec-path-from-shell-initialize)



{
  allowUnfree = true;

  packageOverrides = pkgs: with pkgs; {
    myPackages = pkgs.buildEnv {
      name = "my-packages";
      paths = [
        # Network utils
        transmission-gtk
        clipgrab

        # Browsers and email client
        vivaldi
        chromium
        qutebrowser
        opera
        #torbrowser
        thunderbird-bin

        #### dev/lang
        # NodeJS
        nodejs_latest
        yarn

        # Haskell
        haskellPackages.ghc
        cabal2nix
        stack
        hlint
        
        # Erlang
        erlang
        elixir
        rebar3

        # Python
        python3
        python38Packages.virtualenv

        # Rust
        rustc
        cargo

        # Go
        go

        # Messengers, voip, etc
        zoom-us
        slack
        skype
        tdesktop
        vk-messenger
        teams
        linphone

        # Virtual
        qemu
        virtmanager

        # Develop utils
        terminator
        tmux
        docker-compose
        git
        gitAndTools.gitflow
        gitg
        gcc
        kubectl
        awscli
        eksctl
        ansible

        # Editors and IDE
        netbeans
        atom
        vscode-with-extensions
        jetbrains.goland
        jetbrains.datagrip
        jupyter

        # Misc utils
        dmenu
        clutter
        gnome3.clutter
        gnome3.clutter-gtk
        feh
        trayer
        python37Packages.pip
        speedtest-cli
        yandex-disk
        dropbox
        translate-shell
        krita
        zip
        unzip
        unrar
        file
        

        # Databases
        postgresql
        mongodb
        mongodb-tools
        clickhouse

        # Media
        mesa
        vlc
        # audacious
        v4l_utils
        
        # Make music
        ardour
        kdenlive
        frei0r
        pulseeffects

        # Games
        playonlinux

        # Office
        libreoffice

        # System utils
        dcfldd
        gparted
        shutter
        broot
        mimeo
        aumix
        brightnessctl
        lsb-release
      ];
    };
  };
}

function tmuxns --description 'add tmux session'
tmux new-session -s $argv; and echo "    - $argv" >> ~/.tmux_sessions.yml;
end

#!/usr/bin/env fish

lock=$argv[1] #timeout in seconds
off=$argv[2] #timeout in seconds
    
swayidle -w \
    timeout $lock 'swaylock -f -c 000000' \
    timeout $off 'swaymsg "output * dpms off"' \
        resume 'swaymsg "output * dpms on"' \
    before-sleep 'swaylock -f -c 000000' &
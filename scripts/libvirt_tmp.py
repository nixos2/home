#!/usr/bin/env python
import yaml
import os
import sys

def start(params):
    for param in params:
        os.system("sudo sed -i 's/# "+param+"=y # is not set/"+param+"=y/g' /usr/src/linux/.config")
        print(param)

with open('/home/apterion/libvirt_kern_deps.yml') as f:
    src = yaml.safe_load(f)
    deps, = list(map(lambda x: x[1], sorted(src.items())))
start(deps)

#!/usr/bin/env python
import sys
import os
if len(sys.argv) > 2:
  ssid=sys.argv[1]
  passwd=sys.argv[2]
  os.system("nmcli device wifi connect "+ssid+" password "+passwd)
else:
  print("Usage: ./wifi.py <SSID> <PASSWORD>")
  print("Available networks:")
  os.system("nmcli connection | grep wifi | cut -d ' ' -f1")


#nmcli device wifi connect TP-Link_8524 password "61125582"

#!/usr/bin/env fish

nmcli device wifi list | grep (nmcli device | grep wlp0s20f3 | awk -F ' ' '{print $4}') | awk -F ' ' '{print $2 " " $8}' > /tmp/wifi_status

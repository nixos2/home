(define-package "realgud-node-inspect" "20190523.1251" "Realgud front-end to newer \"node inspect\""
  '((realgud "1.4.5")
    (load-relative "1.2")
    (cl-lib "0.5")
    (emacs "24"))
  :authors
  '(("Rocky Bernstein" . "rocky@gnu.org"))
  :maintainer
  '("Rocky Bernstein" . "rocky@gnu.org")
  :url "http://github.com/realgud/realgud-node-inspect")
;; Local Variables:
;; no-byte-compile: t
;; End:

;;; -*- no-byte-compile: t -*-
(define-package "distel-completion-lib" "20160816.406" "Completion library for Erlang/Distel" 'nil :commit "2ba4eea51cecfa75cf62f58180460ee9ff43131f" :url "github.com/sebastiw/distel-completion" :keywords '("erlang" "distel" "completion"))

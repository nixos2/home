;;; hasky-stack-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "hasky-stack" "hasky-stack.el" (0 0 0 0))
;;; Generated autoloads from hasky-stack.el

(autoload 'hasky-stack-execute "hasky-stack" "\
Show the root-level popup allowing to choose and run a Stack command.

\(fn)" t nil)

(autoload 'hasky-stack-new "hasky-stack" "\
Initialize the current directory by using a Stack template.

PROJECT-NAME is the name of project and TEMPLATE is quite
obviously template name.

\(fn PROJECT-NAME TEMPLATE)" t nil)

(autoload 'hasky-stack-package-action "hasky-stack" "\
Open a popup allowing to install or request information about PACKAGE.

This functionality currently relies on existence of ‘tar’
command.  This means that it works on Posix systems, but may have
trouble working on Windows.  Please let me know if you run into
any issues on Windows and we'll try to work around (I don't have
a Windows machine).

\(fn PACKAGE)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "hasky-stack" '("hasky-stack-")))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; hasky-stack-autoloads.el ends here

;ELC   
;;; Compiled
;;; in Emacs version 26.3
;;; with all optimizations.

;;; This file contains utf-8 non-ASCII characters,
;;; and so cannot be loaded into Emacs 22 or earlier.
(and (boundp 'emacs-version)
     (< (aref emacs-version (1- (length emacs-version))) ?A)
     (string-lessp emacs-version "23")
     (error "`%s' was compiled for Emacs 23 or later" #$))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(byte-code "\300\301!\210\300\302!\210\300\303!\210\300\304!\210\300\305!\210\300\306!\210\300\307!\210\300\310!\210\300\311!\207" [require format-spec telega-core telega-tdlib telega-customize telega-media telega-ffplay telega-vvnote telega-util telega-tme] 2)
(defvar telega-msg-button-map (byte-code "\301 \302\"\210\303\304\305#\210\303\306\307#\210\303\310\311#\210\303\312\313#\210\303\314\315#\210\303\316\317#\210\303\320\321#\210\303\322\323#\210\303\324\325#\210\303\326\327#\210\303\330\327#\210\303\331\332#\210\303\333\334#\210\303\335\336#\210\303\337\340#\210\303\341\327#\210\211\207" [button-map make-sparse-keymap set-keymap-parent define-key [remap self-insert-command] ignore "n" telega-button-forward "p" telega-button-backward "m" telega-msg-mark-toggle "U" telega-chatbuf-unmark-all "i" telega-describe-message "r" telega-msg-reply "e" telega-msg-edit "f" telega-msg-forward-marked-or-at-point "d" telega-msg-delete-marked-or-at-point "k" "l" telega-msg-redisplay "=" telega-msg-diff-edits "R" telega-msg-resend "S" telega-msg-save ""] 5))
(define-button-type 'telega-msg :supertype 'telega :inserter telega-inserter-for-msg-button 'read-only t 'keymap telega-msg-button-map 'action 'telega-msg-button--action)
#@58 Action to take when chat BUTTON is pressed.

(fn BUTTON)
(defalias 'telega-msg-button--action #[257 "\300!\301\302\"\204 \303\304!\210\211\203 \211!\202 \305!\207" [telega-msg-at button-get :action cl--assertion-failed msg telega-msg-open-content] 5 (#$ . 1661)])
#@42 Pretty printer for MSG button.

(fn MSG)
(defalias 'telega-msg--pp #[257 "\304!\305\"\203 \306\307\"\203 \310\202] \311!\203 \312\202] \305	\"\203\\ `\313V\203\\ \314`\315Z!\211\205S \316!?\205S \306\317\"\306\317\"=\205S \306\320\"\306\320\"Z\nW\262\203\\ \321\202] \322p\323\324\325$\210\326\327!)\207" [telega-chat-show-deleted-messages-for telega-chat-group-messages-for telega-chat-group-messages-timespan telega--current-buffer telega-msg-chat telega-chat-match-p plist-get :telega-is-deleted-message telega-ins--message-deleted telega-msg-ignored-p telega-ins--message-ignored 3 telega-msg-at 2 telega-msg-special-p :sender_user_id :date telega-ins--message-no-header telega-ins--message telega-button--insert telega-msg :inserter telega-ins "\n"] 8 (#$ . 1939)])
#@63 Pretty printer for MSG button shown in root buffer.

(fn MSG)
(defalias 'telega-msg-root--pp #[257 "\300\301!C!\205 \302\303\304\305\306\307&\210\310\311!\207" [telega-filter-chats telega-msg-chat telega-button--insert telega-msg :inserter telega-ins--root-msg :action telega-msg-goto-highlight telega-ins "\n"] 8 (#$ . 2734)])
#@325 Get message by CHAT-ID and MSG-ID pair.
If LOCALLY-P is non-nil, then do not perform request to telega-server.
If CALLBACK is specified and message is not available at the
moment, then fetch message asynchronously and call the CALLBACK
function with one argument - message.

(fn CHAT-ID MSG-ID &optional LOCALLY-P CALLBACK)
(defalias 'telega-msg--get #[1026 "\304!\203 =\203 p\202 \305	\306\307$\310!\205) r\211q\210\311\211\312!+\266\202\2043 \211\2037 \211\202< \313#\207" [telega-chatbuf--chat telega--chat-buffers buffer-undo-list inhibit-read-only telega-chat-get cl-find :test #[514 "r\211q\210)=\207" [telega-chatbuf--chat] 4 "\n\n(fn VAL BUF)"] buffer-live-p t telega-chatbuf--msg telega--getMessage] 10 (#$ . 3073)])
(byte-code "\300\301\302\303#\300\207" [function-put telega-msg--get lisp-indent-function 3] 4)
#@54 Return current message at point.

(fn &optional POS)
(defalias 'telega-msg-at #[256 "\301\206 `!\211\205 \211\302\303\")\304=\205 \302\305\"\207" [button button-at button-get type telega-msg :value] 5 (#$ . 3919)])
#@58 Return non-nil if MSG is of MSG-TYPE.

(fn MSG-TYPE MSG)
(defalias 'telega-msg-type-p #[514 "\300\301\211\302\"\303\"!=\207" [intern plist-get :content :@type] 7 (#$ . 4147)])
#@89 Return chat for the MSG.
Return nil for deleted messages.

(fn MSG &optional OFFLINE-P)
(defalias 'telega-msg-chat #[513 "\300\301\302\"\"\207" [telega-chat-get plist-get :chat_id] 6 (#$ . 4331)])
#@213 Return message MSG replying to.
If LOCALLY-P is non-nil, then do not perform any requests to telega-server.
If CALLBACK is specified, then get reply message asynchronously.

(fn MSG &optional LOCALLY-P CALLBACK)
(defalias 'telega-msg-reply-msg #[769 "\300\301\"\211\302U?\205 \303\300\304\"$\207" [plist-get :reply_to_message_id 0 telega-msg--get :chat_id] 9 (#$ . 4537)])
(byte-code "\300\301\302\303#\300\207" [function-put telega-msg-reply-msg lisp-indent-function 2] 4)
#@49 Goto message MSG.

(fn MSG &optional HIGHLIGHT)
(defalias 'telega-msg-goto #[513 "\300\301!\302\303\"#\207" [telega-chat--goto-msg telega-msg-chat plist-get :id] 7 (#$ . 5023)])
#@47 Goto message MSG and hightlight it.

(fn MSG)
(defalias 'telega-msg-goto-highlight #[257 "\300\301\"\207" [telega-msg-goto hightlight] 4 (#$ . 5210)])
#@44 Open content of the message MSG.

(fn MSG)
(defalias 'telega--openMessageContent #[257 "\300\301\302\303\304\303\"\305\304\306\"\257!\207" [telega-server--send :@type "openMessageContent" :chat_id plist-get :message_id :id] 10 (#$ . 5368)])
#@49 Open content for sticker message MSG.

(fn MSG)
(defalias 'telega-msg-open-sticker #[257 "\305\211\211\306\"\307\"\310\"\211\311\230\203 \312\313!\202\256 \314\315\"\211\203' \316\317!\"\202\254 \320\211\223\210\321	B\322\nB\323 \324\325!\211\320\211\211\262rq\210\326\320\"\262)\327\330!\203W \330\"\210)\266*\331 \210\320\211\223\210\321	B\322\nB\323 \324\325!\211\320\211\fq\210\332\333!\210\334\335!\262rq\210\326\320\"\262)\327\330!\203\227 \330\"\210)\266*\314\320\336\337\340\341\342\n!\343\"\344\345%#\262\207" [help-window-point-marker temp-buffer-window-setup-hook temp-buffer-window-show-hook help-window-old-frame standard-output plist-get :content :sticker :set_id "0" message "Sticker has no associated stickerset" telega-stickerset-get locally telega-describe-stickerset telega-msg-chat nil help-mode-setup help-mode-finish selected-frame temp-buffer-window-setup "*Telegram Sticker Set*" temp-buffer-window-show functionp help-window-setup redisplay cursor-sensor-mode 1 telega-ins "Loading stickerset..." make-byte-code 257 "\301\302\300!\"\207" vconcat vector [telega-describe-stickerset telega-msg-chat] 5 "\n\n(fn STICKERSET)"] 12 (#$ . 5619)])
#@63 Open content for video message MSG.

(fn MSG &optional VIDEO)
(defalias 'telega-msg-open-video #[513 "\211\206 \300\211\301\"\302\"\303\302\"\304\305\306\307\310\311\312!\313\"\314\315%#\207" [plist-get :content :video telega-file--renew telega-file--download 32 make-byte-code 257 "\302\300!\210\211\303\211\304\"\305\"\262\205+ \303\211\300\306\"\307\"\203 \310\300!\210\311\312\303\211\304\"\313\"\314	$\207" vconcat vector [telega-video-ffplay-args telega-msg-redisplay plist-get :local :is_downloading_completed :content :is_secret telega--openMessageContent apply telega-ffplay-run :path nil] 7 "\n\n(fn FILE)"] 13 (#$ . 6823)])
#@47 Open content for audio message MSG.

(fn MSG)
(defalias 'telega-msg-open-audio #[257 "\300\211\301\"\302\"\303\302\"\300\304\"\305!\205 \306!\307\310\"\203% \311!\202C \307\312\"\2032 \313!\202C \314\315\316\317\320\321\322\f!\323\"\324\325%#\262\207" [plist-get :content :audio telega-file--renew :telega-ffplay-proc process-live-p process-status eql run telega-ffplay-pause stop telega-ffplay-resume telega-file--download 32 make-byte-code 257 "\301\300!\210\211\302\211\303\"\304\"\262\205, \305\300\306\307\302\211\303\"\310\"\311\312\313\314\315\300!\316\"\317\320%\321##\207" vconcat vector [telega-msg-redisplay plist-get :local :is_downloading_completed plist-put :telega-ffplay-proc telega-ffplay-run :path make-byte-code 257 "\301\300!\207" vconcat vector [telega-msg-redisplay] 3 "\n\n(fn PROC)" "-nodisp"] 12 "\n\n(fn FILE)"] 14 (#$ . 7474)])
#@62 Return callback to be used in `telega-ffplay-run'.

(fn MSG)
(defalias 'telega-msg-voice-note--ffplay-callback #[257 "\300\301\302\303\304!\305\"\306\307%\207" [make-byte-code 257 "\302\300!\210\303!\204 \304\305\306\300!\"\210\307!\310=\2052 	\2052 \311\305\"\210\312\300\313\314\315\"\"\211\2050 \316!\262\207" vconcat vector [telega-vvnote-voice-play-next telega-msg-redisplay process-live-p telega-msg-activate-voice-note nil telega-msg-chat process-status exit set-process-plist telega-chatbuf--next-msg apply-partially telega-msg-type-p messageVoiceNote telega-msg-open-content] 6 "\n\n(fn PROC)"] 7 (#$ . 8352)])
#@51 Open content for voiceNote message MSG.

(fn MSG)
(defalias 'telega-msg-open-voice-note #[257 "\300\211\301\"\302\"\303\304\"\300\305\"\306!\205 \307!\310\311\"\203% \312!\202C \310\313\"\2032 \314!\202C \315\316\317\320\321\322\323\f!\324\"\325\326%#\262\207" [plist-get :content :voice_note telega-file--renew :voice :telega-ffplay-proc process-live-p process-status eql run telega-ffplay-pause stop telega-ffplay-resume telega-file--download 32 make-byte-code 257 "\301\300!\210\211\302\211\303\"\304\"\262\205' \305\300\306\307\302\211\303\"\310\"\311\300!\312##\210\313\300!\207" vconcat vector [telega-msg-redisplay plist-get :local :is_downloading_completed plist-put :telega-ffplay-proc telega-ffplay-run :path telega-msg-voice-note--ffplay-callback "-nodisp" telega-msg-activate-voice-note] 9 "\n\n(fn FILE)"] 14 (#$ . 8985)])
#@59 Callback for video note playback.

(fn PROC FILENAME MSG)
(defalias 'telega-msg-video-note--callback #[771 "\300!\301\302\303\"!\206 \304\302\211\211\305\"\306\"\307\"_\302\310\"\211\245\311\312\205, \313	\"#\210\314!\207" [process-plist float plist-get :nframes 30.0 :content :video_note :duration :frame-num plist-put :telega-ffplay-frame telega-vvnote-video--svg telega-msg-redisplay] 13 (#$ . 9844)])
#@181 Open content for videoNote message MSG.
If called with `\[universal-argument]' prefix, then open with
external player even if `telega-video-note-play-inline' is
non-nil.

(fn MSG)
(defalias 'telega-msg-open-video-note #[257 "\300\211\301\"\302\"\303\304\"\300\305\"\306!\205 \307!\310\311\"\203% \312!\202C \310\313\"\2032 \314!\202C \315\316\317\320\321\322\323\f!\324\"\325\326%#\262\207" [plist-get :content :video_note telega-file--renew :video :telega-ffplay-proc process-live-p process-status eql run telega-ffplay-pause stop telega-ffplay-resume telega-file--download 32 make-byte-code 257 "\303\300!\210\211\304\211\305\"\306\"\262\205; \304\211\305\"\307\"	\2035 \n\2045 \310\300\311\312\313\314\315\316\317\320\321\257\322\300$#\2029 \323\324\"\262\207" vconcat vector [telega-video-note-play-inline current-prefix-arg telega-msg-redisplay plist-get :local :is_downloading_completed :path plist-put :telega-ffplay-proc telega-ffplay-to-png "-vf" "scale=120:120" "-f" "alsa" "default" "-vsync" "0" telega-msg-video-note--callback telega-ffplay-run nil] 14 "\n\n(fn FILE)"] 14 (#$ . 10268)])
#@63 Open content for photo message MSG.

(fn MSG &optional PHOTO)
(defalias 'telega-msg-open-photo #[513 "\300\206\f \301\211\302\"\303\"\"\207" [telega-photo--open plist-get :content :photo] 7 (#$ . 11394)])
#@66 Callback for inline animation playback.

(fn PROC FILENAME ANIM)
(defalias 'telega-animation--ffplay-callback #[771 "\300\301#\210\302\303B\304\"\210\305 \207" [plist-put :telega-ffplay-frame-filename telega-media--image-update telega-animation--create-image nil force-window-update] 7 (#$ . 11608)])
#@200 Open content for animation message MSG.
If called with `\[universal-argument]' prefix, then open with
external player even if `telega-animation-play-inline' is
non-nil.

(fn MSG &optional ANIMATION)
(defalias 'telega-msg-open-animation #[513 "\211\206 \300\211\301\"\302\"\303\302\"\300\304\"\305!\205 \306!\307\310\"\203) \311!\202I \307\312\"\2036 \313!\202I \314\315\316\317\320\321\322\f\"\323\"\324\325%#\262\207" [plist-get :content :animation telega-file--renew :telega-ffplay-proc process-live-p process-status eql run telega-ffplay-pause stop telega-ffplay-resume telega-file--download 32 make-byte-code 257 "\304\300!\210\211\305\211\306\"\307\"\262\2055 \305\211\306\"\310\"\n\203- \204- \311\300\312\313\314\315\301$#\2023 \316\314\317\320$\262\207" vconcat vector [telega-animation-play-inline current-prefix-arg telega-msg-redisplay plist-get :local :is_downloading_completed :path plist-put :telega-ffplay-proc telega-ffplay-to-png nil telega-animation--ffplay-callback telega-ffplay-run "-loop" "0"] 10 "\n\n(fn FILE)"] 16 (#$ . 11919)])
#@69 Open content for document message MSG.

(fn MSG &optional DOCUMENT)
(defalias 'telega-msg-open-document #[513 "\211\206 \300\211\301\"\302\"\303\302\"\304\305\306\307\310\311\312!\313\"\314\315%#\207" [plist-get :content :document telega-file--renew telega-file--download 32 make-byte-code 257 "\301\300!\210\211\302\211\303\"\304\"\262\205 \305\302\211\303\"\306\"\300\"\207" vconcat vector [telega-msg-redisplay plist-get :local :is_downloading_completed telega-find-file :path] 6 "\n\n(fn FILE)"] 13 (#$ . 13002)])
#@50 Open content for location message MSG.

(fn MSG)
(defalias 'telega-msg-open-location #[257 "\301\211\302\"\303\"\301\304\"\301\305\"\306\307\310\311$\"\312\313\"\207" [telega-location-url-format plist-get :content :location :latitude :longitude format-spec format-spec-make 78 69 telega-browse-url in-web-browser] 11 (#$ . 13536)])
#@49 Open content for contact message MSG.

(fn MSG)
(defalias 'telega-msg-open-contact #[257 "\300\301\211\302\"\303\"!\207" [telega-describe-contact plist-get :content :contact] 6 (#$ . 13882)])
#@81 Open content for message with webpage message MSG.

(fn MSG &optional WEB-PAGE)
(defalias 'telega-msg-open-webpage #[513 "\211\204 \300\211\301\"\302\"\262\300\303\"\203 \304\300\303\"\"\207\300\305\"\203+ \306\300\305\"\"\207\300\307\"\203: \310\300\307\"\"\207\300\311\"\312\230\203R \300\313\"\203R \314\300\313\"\"\207\300\315\"\211\205] \316!\207" [plist-get :content :web_page :video telega-msg-open-video :animation telega-msg-open-animation :document telega-msg-open-document :type "photo" :photo telega-msg-open-photo :url telega-browse-url] 7 (#$ . 14081)])
#@50 Open content for the game message MSG.

(fn MSG)
(defalias 'telega-msg-open-game #[257 "\300\301\302\303\304\211\211\305\"\306\"\307\"F\"\207" [telega--getCallbackQueryAnswer :@type "callbackQueryPayloadGame" :game_short_name plist-get :content :game :short_name] 11 (#$ . 14674)])
#@42 Open content for the poll MSG.

(fn MSG)
(defalias 'telega-msg-open-poll #[257 "\305\211\306\"\307\"\305\310\"?\205\311\211\223\210\312	B\313\nB\314 \315\316!\211\311\211\211\262rq\210\317\311\"\262)\320\321!\203? \321\"\210)\266*\322 \210\311\211\223\210\312	B\313\nB\314 \315\316!\211\311\211\fq\210\323\324!\210\325\326\327\330!r\211q\210\331\332\333\334\335!\336\"\337$\216\325\305\340\"!\210\325\341\342\343\344\305\f\345\"#\346#\210\347 *\262\350\351D\"!\210\325\352!\210\353\305\354\"\311\"\211G\332\211W\205\364 \211\2118\325\355\356\357\360\"\226\305\361\"\342\343\344\305	\362\"#$!\210\363\n\"\211\205\332 \364\365\305\366\"\"\211\203\346 \367!\210\325\370!\210\266\325\370!\266\211T\262\202\250 \266\202\262\262rq\210\317\311\"\262)\320\321!\203\321\"\202\211)\266\203*\207" [help-window-point-marker temp-buffer-window-setup-hook temp-buffer-window-show-hook help-window-old-frame standard-output plist-get :content :poll :is_anonymous nil help-mode-setup help-mode-finish selected-frame temp-buffer-window-setup "*Telega Poll Results*" temp-buffer-window-show functionp help-window-setup redisplay cursor-sensor-mode 1 telega-ins telega-fmt-eval-attrs generate-new-buffer " *temp*" make-byte-code 0 "\301\300!\205	 \302\300!\207" vconcat vector [buffer-name kill-buffer] 2 :question " (" telega-i18n "polls_votes_count" :count :total_voter_count ")" buffer-string :face bold "\n\n" append :options format "%s — %d%% (%s)\n" telega-tl-str :text :vote_percentage :voter_count telega--getPollVoters mapcar telega-user--get :user_ids telega-ins--user-list "\n"] 21 (#$ . 14965)])
#@37 Open message MSG content.

(fn MSG)
(defalias 'telega-msg-open-content #[257 "\300\211\301\"\302\"\204 \303!\210\304\300\211\301\"\305\"!\306\307\"\203$ \310!\202)\306\311\"\2031 \312!\202)\306\313\"\203> \314!\202)\306\315\"\203K \316!\202)\306\317\"\203X \320!\202)\306\321\"\203e \322!\202)\306\323\"\203r \324!\202)\325\326\"\203 \327!\202)\306\330\"\203\214 \331!\202)\306\332\"\203\231 \333!\202)\306\334\"\203\264 \300\211\301\"\335\"\211\205\257 \336\"\262\202)\306\337\"\203\301 \340!\202)\306\341\"\203\316 \342!\202)\306\343\"\203\350 \300\211\301\"\344\"\345\346\"\347!\266\202\202)\306\350\"\203\300\211\301\"\351\"\352\346\"\347!\266\202\202)\306\353\"\203\300\211\301\"\354\"\355\356!\357#\262\202)\360\361\304\300\211\301\"\305\"!\"\207" [plist-get :content :is_secret telega--openMessageContent intern :@type eql messageDocument telega-msg-open-document messageSticker telega-msg-open-sticker messageVideo telega-msg-open-video messageAudio telega-msg-open-audio messageAnimation telega-msg-open-animation messageVoiceNote telega-msg-open-voice-note messageVideoNote telega-msg-open-video-note memql (messagePhoto messageChatChangePhoto) telega-msg-open-photo messageLocation telega-msg-open-location messageContact telega-msg-open-contact messageText :web_page telega-msg-open-webpage messagePoll telega-msg-open-poll messageGame telega-msg-open-game messageChatUpgradeTo :supergroup_id telega--createSupergroupChat force telega-chat--pop-to-buffer messageChatUpgradeFrom :basic_group_id telega--createBasicGroupChat messagePinMessage :message_id telega-chat--goto-msg telega-msg-chat hightlight message "TODO: `open-content' for <%S>"] 9 (#$ . 16612)])
#@70 Track uploading progress for the file associated with MSG.

(fn MSG)
(defalias 'telega-msg--track-file-uploading-progress #[257 "\300!\211\205$ \211\301\211\302\"\303\"\262\205$ \304\305\306\307\310\311!\312\"\313\314%\"\207" [telega-file--used-in-msg plist-get :remote :is_uploading_active telega-file--upload-internal make-byte-code 257 "\301\300!\207" vconcat vector [telega-msg-redisplay] 3 "\n\n(fn FILENOTUSED)"] 10 (#$ . 18359)])
#@40 Title of the message's chat.

(fn MSG)
(defalias 'telega-msg-chat-title #[257 "\300\301!\302\"\207" [telega-chat-title telega-msg-chat with-username] 4 (#$ . 18808)])
#@51 Return sender (if any) for message MSG.

(fn MSG)
(defalias 'telega-msg-sender #[257 "\300\301\"\211\302U?\205 \303!\207" [plist-get :sender_user_id 0 telega-user--get] 4 (#$ . 18982)])
(put 'telega-msg-sender 'byte-optimizer 'byte-compile-inline-expand)
#@50 Return non-nil if sender of MSG is me.

(fn MSG)
(defalias 'telega-msg-by-me-p #[257 "\301\302\"U\207" [telega--me-id plist-get :sender_user_id] 4 (#$ . 19246)])
(put 'telega-msg-by-me-p 'byte-optimizer 'byte-compile-inline-expand)
#@79 Return non-nil if MSG has been already read in CHAT.

(fn MSG &optional CHAT)
(defalias 'telega-msg-seen-p #[513 "\211\204	 \300!\262\301\302\"\301\303\"X\207" [telega-msg-chat plist-get :id :last_read_inbox_message_id] 6 (#$ . 19486)])
(put 'telega-msg-seen-p 'byte-optimizer 'byte-compile-inline-expand)
#@52 Return non-nil if message MSG is marked.

(fn MSG)
(defalias 'telega-msg-marked-p #[257 "\305!\203 =\203 p\202 \306	\307\310$\311!\205) r\211q\210\312\211\f>+\207" [telega-chatbuf--chat telega--chat-buffers buffer-undo-list inhibit-read-only telega-chatbuf--marked-messages telega-msg-chat cl-find :test #[514 "r\211q\210)=\207" [telega-chatbuf--chat] 4 "\n\n(fn VAL BUF)"] buffer-live-p t] 7 (#$ . 19802)])
(put 'telega-msg-marked-p 'byte-optimizer 'byte-compile-inline-expand)
#@145 Return non-nil if MSG is observable in chatbuffer.
CHAT - chat to search message for.
NODE - ewoc node, if known.

(fn MSG &optional CHAT NODE)
(defalias 'telega-msg-observable-p #[769 "\204	 \304!\262\203 =\203 p\202 \305	\306\307$\310!\205C r\211q\210\311\211\2049 \312\313\314\"!\262\205B \315\316!!+\207" [telega-chatbuf--chat telega--chat-buffers buffer-undo-list inhibit-read-only telega-msg-chat cl-find :test #[514 "r\211q\210)=\207" [telega-chatbuf--chat] 4 "\n\n(fn VAL BUF)"] buffer-live-p t telega-chatbuf--node-by-msg-id plist-get :id telega-button--observable-p ewoc-location] 9 (#$ . 20302)])
#@53 Return non-nil if MSG has unread mention.

(fn MSG)
(defalias 'telega-msg-contains-unread-mention-p #[257 "\300\301\"\207" [plist-get :contains_unread_mention] 4 (#$ . 20938)])
#@12 

(fn MSG)
(defalias 'telega-msg-sender-admin-status #[257 "\300\301\302\303\304\303\"F!\305\304\306\"\304\307\"\310\311$\205 \312\207" [telega-server--call :@type "getChatAdministrators" :chat_id plist-get cl-find :sender_user_id :user_ids :test = " (admin)"] 8 (#$ . 21122)])
#@168 Parse TEXT using PARSE-MODE.
PARSE-MODE is one of:
  (list :@type "textParseModeMarkdown" :version 0|1|2)
or
  (list :@type "textParseModeHTML")

(fn TEXT PARSE-MODE)
(defalias 'telega--parseTextEntities #[514 "\300\301\302\303\304\257!\305\303\306\303\307#\206 \310#\207" [telega-server--call :@type "parseTextEntities" :text :parse_mode plist-put telega-tl-str no-props ""] 10 (#$ . 21411)])
#@129 Convert TEXT to `formattedTex' type.
If MARKDOWN is non-nil then format TEXT as markdown.

(fn TEXT &optional MARKDOWN-VERSION)
(defalias 'telega--formattedText #[513 "\211\203 \300\301!\302\303\304F\"\207\302\305\306\307!\310\311\257\207" [telega--parseTextEntities telega-escape-underscores :@type "textParseModeMarkdown" :version "formattedText" :text substring-no-properties :entities []] 8 (#$ . 21819)])
#@108 Return substring of the TEXT.
FROM and TO are passed directly to `substring'.

(fn TEXT FROM &optional TO)
(defalias 'telega-formattedText-substring #[770 "\211C\211\242\204 \211\300\301\"G\240\210\302\303\304\305\306\307\"\310\"\311\312%\300\313\"\"\314\315\301\300\301\"\242O\313\316\307\317\320	\"\"\257\262\207" [plist-get :text mapcar make-byte-code 257 "\302\303\"\211\302\304\"\\\300V\203 \305\202< \301\242V\203 \305\202< \306\302\306\"\307\302\307\"\303\300]\304\301\242^\300\n]Z\257\207" vconcat vector [plist-get :offset :length nil :@type :type] 13 "\n\n(fn ENT)" :entities :@type "formattedText" apply cl-remove-if null] 15 (#$ . 22241)])
#@45 Return non-nil if MSG is ignored.

(fn MSG)
(defalias 'telega-msg-ignored-p #[257 "\301\302\"\2065 \30325 \304!\305\211W\2052 \211\301\306\"\301\307\"\306\"=\203* \310\303\"\210\210\211T\262\202 \266\2020\207" [telega--ignored-messages-ring plist-get :ignored-p found ring-length 0 :id ring-ref throw] 9 (#$ . 22931)])
#@195 Mark message MSG to be ignored (not viewed, notified about) in chats.
By side effect adds MSG into `telega--ignored-messages-ring' to be viewed
with `M-x telega-ignored-messages RET'.

(fn MSG)
(defalias 'telega-msg-ignore #[257 "\302\303\304#\210\305\"\210\306C	\205< r\307\310!q\210\311`\304\"\312\313\314\315\316!\317\"\320$\216db\210\321\322\323\324Q\325 BB\"c)\262)\207" [telega--ignored-messages-ring telega-debug plist-put :ignored-p t ring-insert "IGNORED msg: %S" get-buffer-create "*telega-debug*" copy-marker make-byte-code 0 "\300b\207" vconcat vector [] 1 apply format "%d: " "\n" telega-time-seconds] 10 (#$ . 23266)])
#@159 Function to be used as `telega-chat-pre-message-hook'.
Add it to `telega-chat-pre-message-hook' to ignore messages from
blocked users.

(fn MSG &rest IGNORE)
(defalias 'telega-msg-ignore-blocked-sender #[385 "\300\301\"\211\302U?\205 \300\303\304!!\305\"\205 \306!\207" [plist-get :sender_user_id 0 telega--full-info telega-user--get :is_blocked telega-msg-ignore] 7 (#$ . 23914)])
#@31 Unmark message MSG.

(fn MSG)
(defalias 'telega-msg-unmark #[257 "\305!\203 =\203 p\202 \306	\307\310$\311!\205` r\211q\210\312\211\305!\2037 =\2037 p\202= \306	\307\313$\311!\205O r\211q\210\312\211\f>+\266\203\205_ \314\f\"\315 \210\316!+\207" [telega-chatbuf--chat telega--chat-buffers buffer-undo-list inhibit-read-only telega-chatbuf--marked-messages telega-msg-chat cl-find :test #[514 "r\211q\210)=\207" [telega-chatbuf--chat] 4 "\n\n(fn VAL BUF)"] buffer-live-p t #[514 "r\211q\210)=\207" [telega-chatbuf--chat] 4 "\n\n(fn VAL BUF)"] delq telega-chatbuf-mode-line-update telega-msg-redisplay] 10 (#$ . 24307)])
#@43 Toggle mark of the message MSG.

(fn MSG)
(defalias 'telega-msg-mark-toggle #[257 "\305!\203 =\203 p\202 \306	\307\310$\311!\205E r\211q\210\312\211\f>\2033 \313\f\"\2029 \fB\211\314 \210\315!\210\316\317\320\"+\207" [telega-chatbuf--chat telega--chat-buffers buffer-undo-list inhibit-read-only telega-chatbuf--marked-messages telega-msg-chat cl-find :test #[514 "r\211q\210)=\207" [telega-chatbuf--chat] 4 "\n\n(fn VAL BUF)"] buffer-live-p t delq telega-chatbuf-mode-line-update telega-msg-redisplay telega-button-forward 1 telega-msg-at] 7 (#$ . 24960) (byte-code "\300`!C\207" [telega-msg-at] 2)])
#@173 Pin message MSG.
If prefix arg is specified, then do not notify all the users about pin.
If MSG is already pinned, then unpin it.

(fn MSG &optional DISABLE-NOTIFICATIONS)
(defalias 'telega-msg-pin #[513 "\300!\301\302\"\301\303\"\204 \304\305!\210\301\306\"\301\307\"=\203$ \310!\202( \311\"\207" [telega-msg-chat plist-get :permissions :can_pin_messages user-error "Can't pin messages in this chat" :pinned_message_id :id telega--unpinChatMessage telega--pinChatMessage] 8 (#$ . 25588) (byte-code "\301`!D\207" [current-prefix-arg telega-msg-at] 2)])
#@56 Save messages's MSG media content to a file.

(fn MSG)
(defalias 'telega-msg-save #[257 "\300\301\"\302\300\303\"!\304\305\"\203! \300\211\301\"\306\"\307\306\"\262\202\275 \304\310\"\2037 \311\300\312\"!\307\312\"\262\202\275 \304\313\"\203N \300\211\301\"\314\"\307\314\"\262\202\275 \304\315\"\203e \300\211\301\"\316\"\307\316\"\262\202\275 \304\317\"\203| \300\211\301\"\320\"\307\321\"\262\202\275 \304\322\"\203\223 \300\211\301\"\323\"\307\316\"\262\202\275 \304\324\"\203\252 \300\211\301\"\325\"\307\325\"\262\202\275 \300\326\"\211\203\270 \327\330!\202\273 \331\332!\262\262\211\204\307 \333\334!\210\335\336\337\340\341\342\343\n!\344\"\345\346%#\207" [plist-get :content intern :@type eql messageDocument :document telega-file--renew messagePhoto telega-photo--highres :photo messageAudio :audio messageVideo :video messageVoiceNote :voice_note :voice messageVideoNote :video_note messageAnimation :animation :web_page error "TODO: Save web-page" user-error "No file associated with message" cl--assertion-failed file telega-file--download 32 make-byte-code 257 "\302\300!\210\211\303\211\304\"\305\"\262\2051 \303\211\304\"\306\"\307!\310\311	\312\211\312&\313\"\210\314\315\316\"!\266\203\207" vconcat vector [default-directory telega-msg-redisplay plist-get :local :is_downloading_completed :path file-name-nondirectory read-file-name "Save to file: " nil copy-file message format "Wrote %s"] 10 "\n\n(fn DFILE)"] 12 (#$ . 26157) (byte-code "\300`!C\207" [telega-msg-at] 2)])
#@45 Show info about message at point.

(fn MSG)
(defalias 'telega-describe-message #[257 "\306\211\223\210\307	B\310\nB\311 \312\313!\211\306\211\211\262rq\210\314\306\"\262)\315\316!\2030 \316\"\210)\266*\317 \210\306\211\223\210\307	B\310\nB\311 \312\313!\211\306\211\fq\210\320\321!\210\322\323\"\322\324\"\325\326!\210\327\322\330\"\331\"\210\325\332\333\"!\210\325\332\334\"!\210\322\335\"\211\336U\204\227 \325\337!\210\340\341\342!!\343\344B#\210\325\331!\210\210\345!\3461\276 \347\350\"\203\255 \351\"\202\272 \352\353\"\350=\205\272 \354\"0\202\300 \210\306\211\203\335 \325\355!\210\356\357\360`\325!\210`\361\362\"$!\210\325\331!\210\266\325\363!\210\364!\357\340\361\362\365##\266\325\331!\210\203\325\332\366#!\210\205\3068\325\332\367\"!)\266\202\262rq\210\314\306\"\262)\315\316!\203-\316\"\202.\211)\266\203*\207" [help-window-point-marker temp-buffer-window-setup-hook temp-buffer-window-show-hook help-window-old-frame standard-output telega-debug nil help-mode-setup help-mode-finish selected-frame temp-buffer-window-setup "*Telegram Message Info*" temp-buffer-window-show functionp help-window-setup redisplay cursor-sensor-mode 1 plist-get :chat_id :id telega-ins "Date(ISO8601): " telega-ins--date-iso8601 :date "\n" format "Chat-id: %d\n" "Message-id: %d\n" :sender_user_id 0 "Sender: " insert-text-button telega-user--name telega-user--get :telega-link user telega-chat-get (error) telega-chat-public-p supergroup telega--getPublicMessageLink telega-chat--type no-interpret telega--getMessageLink "Link: " button-at apply make-text-button telega-link-props url "Internal Link: " telega-tme-internal-link-to link "MsgSexp: (telega-msg--get %d %d)\n" "\nMessage: %S\n" print-length] 16 (#$ . 27700) (byte-code "\300`!C\207" [telega-msg-at] 2)])
#@45 Display all messages that has been ignored.
(defalias 'telega-ignored-messages #[0 "\306\211\223\210\307	B\310\nB\311 \312\313!\211\306\211\fq\210\314!\211\2051 \211@\315\316\317\320$\210A\266\202\202 \262\262rq\210\321\306\"\262)\322\323!\203M \323\"\202N \211)\266\203*\207" [help-window-point-marker temp-buffer-window-setup-hook temp-buffer-window-show-hook help-window-old-frame standard-output telega--ignored-messages-ring nil help-mode-setup help-mode-finish selected-frame temp-buffer-window-setup " *Telegram Ignored Messages*" ring-elements telega-button--insert telega-msg :inserter telega-ins--message-ignored-list temp-buffer-window-show functionp help-window-setup] 10 (#$ . 29533) nil])
#@42 Display edits to MSG user did.

(fn MSG)
(defalias 'telega-msg-diff-edits #[257 "\305\306\"\307U\203 \310\311!\210\312\313\314!\315\211\316\317\320!\305\321\"\211\307U?\205( \322!\266\202C&\323#\324\237#\211\203@ \204D \310\325!\210\315\211\223\210\326	B\327\nB\330 \331\332!\211\315\211\211\262rq\210\333\315\"\262)\334\335!\203t \335\"\210)\266*\336 \210\315\211\223\210\326	B\327\nB\330 \331\332!\211\315\211\fq\210\337\340!\210\341\342\343\344!r\211q\210\345\307\346\347\350!\351\"\352$\216\341\353!\210\354 *\262\355\356\357!D\"!\210\341\360!\210\361\305\362\"!\210\341\363!\210\341\342\343\344!r\211q\210\345\307\346\347\350!\364\"\352$\216\341\365!\210\354 *\262\355\356\366!D\"!\210\341\360!\210\361\305\306\"!\210\341\363!\210\341\367!\210\341\370\343\344!r\211q\210\345\307\346\347\350!\371\"\352$\216\372!\210\354 *\262\343\344!r\211q\210\345\307\346\347\350!\373\"\352$\216\372	!\210\354 *\262\374#!\262rq\210\333\315\"\262)\334\335!\203a\335\"\202b\211)\266\203*\266\203\207" [help-window-point-marker temp-buffer-window-setup-hook temp-buffer-window-show-hook help-window-old-frame standard-output plist-get :edit_date 0 user-error "Message was not edited" #[771 "\300\211\301\300\302\"\303\304\305\306\307\310\f!\311\"\312\313%$\314\"\"\207" [plist-get cl-find :id :key make-byte-code 257 "\301\211\211\302\"\300\"\303\"\207" vconcat vector [plist-get :action :id] 6 "\n\n(fn TL-OBJ279)" :action] 15 "\n\n(fn MSG ACCESOR EVENTS)"] telega--getChatEventLog telega-msg-chat nil 50 telega-chatevent-log-filter :message_edits :sender_user_id telega-user--get :new_message :old_message "Can't find message edit in last 50 edits" help-mode-setup help-mode-finish selected-frame temp-buffer-window-setup "*Telega Message Diff*" temp-buffer-window-show functionp help-window-setup redisplay cursor-sensor-mode 1 telega-ins telega-fmt-eval-attrs generate-new-buffer " *temp*" make-byte-code "\301\300!\205	 \302\300!\207" vconcat vector [buffer-name kill-buffer] 2 "Orig" buffer-string :face ansi-color-get-face-1 31 " message at: " telega-ins--date-iso8601 :date "\n" [buffer-name kill-buffer] "Edit" 32 "-- Diff --\n" telega-diff-wordwise [buffer-name kill-buffer] telega-ins--content [buffer-name kill-buffer] colorize] 18 (#$ . 30258) (byte-code "\300`!C\207" [telega-msg-at] 2)])
(provide 'telega-msg)

;ELC   
;;; Compiled
;;; in Emacs version 26.3
;;; with all optimizations.

;;; This file contains utf-8 non-ASCII characters,
;;; and so cannot be loaded into Emacs 22 or earlier.
(and (boundp 'emacs-version)
     (< (aref emacs-version (1- (length emacs-version))) ?A)
     (string-lessp emacs-version "23")
     (error "`%s' was compiled for Emacs 23 or later" #$))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(byte-code "\300\301!\210\300\302!\210\300\303!\210\300\304!\210\300\305!\210\300\306!\210\300\307!\210\300\310!\207" [require cl-lib url-util visual-fill-column telega-server telega-ins telega-media telega-tme telega-customize] 2)
#@29 History of viewed webpages.
(defvar telega-webpage-history nil (#$ . 640))
#@62 Nth element in `telega-webpage-history' we currently active.
(defvar telega-webpage-history--index 0 (#$ . 721))
#@56 Bind this to non-nil to ignore pushing to the history.
(defvar telega-webpage-history--ignore nil (#$ . 840))
#@45 Bind to non-nil to strip trailing newlines.
(defvar telega-webpage-strip-nl nil (#$ . 956))
#@53 URL for the instant view webpage currently viewing.
(defvar telega-webpage--url nil (#$ . 1054))
#@45 Sitename for the webpage currently viewing.
(defvar telega-webpage--sitename nil (#$ . 1157))
#@39 Instant view for the current webpage.
(defvar telega-webpage--iv nil (#$ . 1257))
(defvar telega-webpage--anchors nil)
(defvar telega-webpage--slides nil)
#@53 Push current webpage instant view into the history.
(defalias 'telega-webpage--history-push #[0 "?\205E 	\204 \306\301\307\310\211$\210\n\203# \n@A@\230\203# \nA\202. \nG\fV\203. \311\n!\312=\2038 `\2029 \313\f	F\nB\313\211\207" [telega-webpage-history--ignore telega-webpage--iv telega-webpage-history telega-webpage--url telega-webpage-history-max major-mode cl--assertion-failed "No current instant view" nil butlast telega-webpage-mode 0 telega-webpage--sitename telega-webpage-history--index] 5 (#$ . 1418)])
#@45 Show N's instant view from history.

(fn N)
(defalias 'telega-webpage--history-show #[257 "\211\303Y\203 \211GW\204 \304\305!\210\2118\306\307\310A\"\210\211@b\210)\210\211\211\207" [telega-webpage-history telega-webpage-history--ignore telega-webpage-history--index 0 cl--assertion-failed (and (>= n 0) (< n (length telega-webpage-history))) t apply telega-webpage--instant-view] 5 (#$ . 1949)])
#@52 Goto N previous word in history.

(fn &optional N)
(defalias 'telega-webpage-history-next #[256 "Z\211\301Y\204 \302\303!\210\304!\207" [telega-webpage-history--index 0 error "No next webpage in history" telega-webpage--history-show] 4 (#$ . 2359) "p"])
#@48 Goto N next word in history.

(fn &optional N)
(defalias 'telega-webpage-history-prev #[256 "\\\211	GW\204 \302\303!\210\304!\207" [telega-webpage-history--index telega-webpage-history error "No previous webpage in history" telega-webpage--history-show] 4 (#$ . 2623) "p"])
#@56 Open instant view when BUTTON is pressed.

(fn BUTTON)
(defalias 'telega-msg-button--iv-action #[257 "\300\301\"\302\211\303\"\304\"\305\302\306\"\302\307\"\"\207" [button-get :value plist-get :content :web_page telega-webpage--instant-view :url :site_name] 8 (#$ . 2907)])
#@115 Return instant view for the URL.
Return nil if URL is not available for instant view.

(fn URL &optional PARTIAL)
(defalias 'telega--getWebPageInstantView #[513 "\300\301\302\303\304?\206 \305\257!\211\205  \306\307\301\"!\310=\205  \211\207" [telega-server--call :@type "getWebPageInstantView" :url :force_full :false intern plist-get webPageInstantView] 9 (#$ . 3192)])
(defvar telega-webpage-mode-map (byte-code "\300 \301\302\303#\210\301\304\303#\210\301\305\306#\210\301\307\310#\210\301\311\312#\210\301\313\310#\210\301\314\312#\210\301\315\316#\210\301\317\320#\210\211\207" [make-sparse-keymap define-key "g" telega-webpage-browse-url "w" "c" telega-webpage-copy-url "l" telega-webpage-history-prev "r" telega-webpage-history-next "p" "n" [9] telega-button-forward [backtab] telega-button-backward] 5))
(defvar telega-webpage-mode-hook nil)
(byte-code "\300\301N\204\f \302\300\301\303#\210\304\305!\204 \302\305\306\307#\210\300\207" [telega-webpage-mode-hook variable-documentation put "Hook run after entering Telega-WebPage mode.\nNo problems result if this variable is not bound.\n`add-hook' automatically binds it.  (This is true for all hook variables.)" boundp telega-webpage-mode-map definition-name telega-webpage-mode] 4)
(defvar telega-webpage-mode-map (make-sparse-keymap))
(byte-code "\301\302N\204 \303\301\302\304\305!#\210\306\307!\204 \303\307\310\311#\210\312\313 !\210\307\302N\204- \303\307\302\304\314!#\210\306\300!\204B \303\300\310\311#\210\315\316\300\317\"\210!\210\300\302N\204P \303\300\302\304\320!#\210\303\311\321\322#\210\303\311\323\324#\207" [telega-webpage-mode-abbrev-table telega-webpage-mode-map variable-documentation put purecopy "Keymap for `telega-webpage-mode'." boundp telega-webpage-mode-syntax-table definition-name telega-webpage-mode (lambda (#1=#:def-tmp-var) (defvar telega-webpage-mode-syntax-table #1#)) make-syntax-table "Syntax table for `telega-webpage-mode'." (lambda (#1#) (defvar telega-webpage-mode-abbrev-table #1#)) define-abbrev-table nil "Abbrev table for `telega-webpage-mode'." derived-mode-parent special-mode custom-mode-group telega] 5)
#@266 The mode for instant viewing webpages in telega.
Keymap:
\{telega-webpage-mode-map}

In addition to any hooks its parent mode `special-mode' might have run,
this mode runs the hook `telega-webpage-mode-hook', as the final or penultimate step
during initialization.
(defalias 'telega-webpage-mode #[0 "\306\300!\210\307\310 \210\311\312\310\313N\203 \314\311\313\310\313N#\210\315!\204' \316\317 \"\210\320\f!\211\2035 \211\321 =\203; \322\f\323 \"\210\210\324\325\"\204R =\204R \326\325C#\210\327!\210\330\f!\210\331\332!\210)\333\334!\207" [delay-mode-hooks major-mode mode-name telega-webpage-mode-map telega-webpage-mode-syntax-table telega-webpage-mode-abbrev-table make-local-variable t special-mode telega-webpage-mode "Telega-WebPage" mode-class put keymap-parent set-keymap-parent current-local-map char-table-parent standard-syntax-table set-char-table-parent syntax-table abbrev-table-get :parents abbrev-table-put use-local-map set-syntax-table set-buffer-modified-p nil run-mode-hooks telega-webpage-mode-hook local-abbrev-table telega-webpage-header-line-format header-line-format] 5 (#$ . 5334) nil])
#@52 Copy current webpage URL into clipboard.

(fn URL)
(defalias 'telega-webpage-copy-url #[257 "\300!\210\301\302\"\207" [kill-new message "Copied \"%s\" into clipboard"] 4 (#$ . 6476) (list telega-webpage--url)])
#@40 Browse URL with web browser.

(fn URL)
(defalias 'telega-webpage-browse-url #[257 "\300\301\"\207" [telega-browse-url in-web-browser] 4 (#$ . 6695) (list telega-webpage--url)])
#@42 Goto at anchor NAME position.

(fn NAME)
(defalias 'telega-webpage-goto-anchor #[257 "\301\"A\211\204 \302\303\304\"!\210\211b\207" [telega-webpage--anchors assoc error format "Anchor \"#%s\" not found"] 6 (#$ . 6879)])
#@60 Toggle open/close state of the details block.

(fn BUTTON)
(defalias 'telega-webpage-details-toggle #[257 "\300\301\"\302\303\304\305\304\"?#\306\307\310\311\312\313&\210b\207" [button-get :value telega-button--update-value plist-put :is_open plist-get action telega-webpage-details-toggle :inserter telega-webpage--ins-details :help-echo "Toggle details"] 11 (#$ . 7109) (byte-code "\300`!C\207" [button-at] 2)])
#@57 Inserter for `pageBlockDetails' page block PB.

(fn PB)
(defalias 'telega-webpage--ins-pb-details #[257 "\303p\304\203\f \305\202 \306	!\307\"\210\310\311\312\"!\210\304\313!\210\304\314\315\316!r\211q\210\317\320\321\322\323!\324\"\325$\216\304\326\n\325\245\327\"!\210\330 *\262\331\332D\"!\210\304\313!\210\211\205U \333\334\311\335\"\")\207" [telega--current-buffer telega-symbol-webpage-details telega-webpage-fill-column t telega-ins cdr car " " telega-webpage--ins-rt plist-get :header "\n" telega-fmt-eval-attrs generate-new-buffer " *temp*" make-byte-code 0 "\301\300!\205	 \302\300!\207" vconcat vector [buffer-name kill-buffer] 2 make-string 32 buffer-string :face telega-webpage-strike-through mapc telega-webpage--ins-pb :page_blocks] 11 (#$ . 7536)])
#@66 Return image representing rich text icon for RT.

(fn RT LIMITS)
(defalias 'telega-webpage-rticon--image #[514 "\300\301\"\300\302\"\300\303\"\304A!V\203 A\202 \305!\211AX\204* \306\307!\210\310\311\312\313\314			$\315\"\316\317%\320B\301B\321#\207" [plist-get :document :width :height telega-chars-xheight telega-chars-in-height cl--assertion-failed (<= cheight (cdr limits)) make-byte-code 513 "\304\300\305\"\211\306\211\307\"\310\"\262\203 \311\301\302\303$\202? \306\300\312\"\304\313\"\211\306\211\307\"\310\"\262\2037 \314\303#\202= \315\301\302\303$\266\202\207" vconcat vector [telega-file--renew :document plist-get :local :is_downloading_completed telega-media--create-image :thumbnail :photo telega-thumb--create-image telega-media--progress-svg] 10 "\n\n(fn RTIGNORED &optional FILEIGNORED)" telega-media--image force-update] 16 (#$ . 8314)])
#@63 Add anchor with the NAME to point at POS position.

(fn NAME)
(defalias 'telega-webpage--add-anchor #[257 "\302\303#r	q\210\304 )\203 \241\202  B\211\262B\211\207" [telega-webpage--anchors telega--current-buffer assoc equal point-marker] 5 (#$ . 9208)])
#@30 Insert RichText RT.

(fn RT)
(defalias 'telega-webpage--ins-rt #[257 "\302\303\304\"!\305\306\"\203$ \307\303\310\"!\210\303\311\"\211\205 \312!\262\202\356\305\313\"\203S \303\314\"\315\316\317`\312\303\311\"!\210`\320\321\322\323\324\325P\326\f\327\330\257\n$!\262\202\356\305\331\"\203\202 \303\310\"\315\316\317`\312\303\311\"!\210`\320\321\322\323\324\332P\326\f\327\333\257\n$!\262\202\356\305\334\"\203\222 \335\336\"!\202\356\305\337\"\203\254 \340	\203\242 \341\202\243 \342\343\311\"!!\202\356\305\344\"\203\275 \345\312\303\346\"\"\202\356\305\347\"\203\356 \340\350\351\352!r\211q\210\353\354\355\356\357!\360\"\361$\216\312\303\311\"!\210\362 *\262\363\364D\"!\202\356\305\365\"\203\340\350\351\352!r\211q\210\353\354\355\356\357!\366\"\361$\216\312\303\311\"!\210\362 *\262\363\367D\"!\202\356\305\370\"\203P\340\350\351\352!r\211q\210\353\354\355\356\357!\371\"\361$\216\312\303\311\"!\210\362 *\262\363\372D\"!\202\356\305\373\"\203\201\340\350\351\352!r\211q\210\353\354\355\356\357!\374\"\361$\216\312\303\311\"!\210\362 *\262\363\375D\"!\202\356\305\376\"\203\264\340\350\351\352!r\211q\210\353\354\355\356\357!\377\"\361$\216\312\303\311\"!\210\362 *\262\363\201@ D\"!\202\356\305\201A \"\203\347\303\314\"\315\316\317`\312\303\311\"!\210`\320\321\322\323\324\201B P\326\f\327\330\257\n$!\262\202\356\305\201C \"\203\340\350\351\352!r\211q\210\353\354\355\356\357!\201D \"\361$\216\312\303\311\"!\210\362 *\262\363\201E D\"!\202\356\305\201F \"\203b`\340\350\351\352!r\211q\210\353\354\355\356\357!\201G \"\361$\216\312\303\311\"!\210\362 *\262\363\201H D\"!\201I `\201J #\210\262\202\356\305\201K \"\203\246`\340\350\351\352!r\211q\210\353\354\355\356\357!\201L \"\361$\216\312\303\311\"!\210\362 *\262\363\201M D\"!\201I `\201N #\210\262\202\356\305\201O \"\203\335\340\350\351\352!r\211q\210\353\354\355\356\357!\201P \"\361$\216\312\303\311\"!\210\362 *\262\363\201Q D\"!\202\356\201R \201S \201T #\205\356\201U \207" [telega-photo-maxsize telega-webpage-strip-nl intern plist-get :@type eql richTextAnchor telega-webpage--add-anchor :name :text telega-webpage--ins-rt richTextReference :url button-at apply make-text-button face telega-link action telega-button--action :help-echo "Reference: " :value :action telega-browse-url richTextAnchorLink "Anchor: #" telega-webpage-goto-anchor richTextIcon telega-ins--image-slices telega-webpage-rticon--image richTextPlain telega-ins telega-strip-newlines identity telega-tl-str richTexts mapc :texts richTextBold telega-fmt-eval-attrs generate-new-buffer " *temp*" make-byte-code 0 "\301\300!\205	 \302\300!\207" vconcat vector [buffer-name kill-buffer] 2 buffer-string :face bold richTextItalic [buffer-name kill-buffer] italic richTextUnderline [buffer-name kill-buffer] underline richTextStrikethrough [buffer-name kill-buffer] telega-webpage-strike-through richTextFixed [buffer-name kill-buffer] telega-webpage-fixed richTextUrl "URL: " richTextEmailAddress [buffer-name kill-buffer] link richTextSubscript [buffer-name kill-buffer] (:height 0.5) add-text-properties (display (raise -0.25)) richTextSuperscript [buffer-name kill-buffer] (:height 0.5) (display (raise 0.75)) richTextMarked [buffer-name kill-buffer] region error "cl-ecase failed: %s, %s" (richTextAnchor richTextReference richTextAnchorLink richTextIcon richTextPlain richTexts richTextBold richTextItalic richTextUnderline richTextStrikethrough richTextFixed richTextUrl richTextEmailAddress richTextSubscript richTextSuperscript richTextMarked) nil] 18 (#$ . 9479)])
#@65 Inserter for pageBlockRelatedArticle PAGEBLOCK.

(fn PAGEBLOCK)
(defalias 'telega-webpage--ins-related-article #[257 "\301\302\"\211\205 \303\304\305B\"\301\306\"\301\307\"\301\310\"\301\311\"\312\313\314\315!r\211q\210\316\317\320\321\322!\323\"\324$\216\203? \325\317\"\210\312\313\314\315!r\211q\210\316\317\320\321\322!\326\"\324$\216\203a \312	!\210\202f \312\n!\210\327 *\262\330\331D\"!\210\312\332!\210\203\200 \325\333\"\210\312\313\314\315!r\211q\210\316\317\320\321\322!\334\"\324$\216\203\242 \312!\210\202\246 \312\335!\210\317U\203\261 \336 \262\312\337!\210\340!\210\327 *\262\330\341D\"!\210\312\332!\210\203\324 \325\324\"\210\312\313\314\315!r\211q\210\316\317\320\321\322!\342\"\324$\216\312\n!\210\327 *\262\330\343D\"!\210\327 *\262\344\345\346F\"!\207" [telega-webpage-fill-column plist-get :photo telega-photo--image 10 3 :url :title :author :publish_date telega-ins telega-fmt-eval-attrs generate-new-buffer " *temp*" make-byte-code 0 "\301\300!\205	 \302\300!\207" vconcat vector [buffer-name kill-buffer] 2 telega-ins--image [buffer-name kill-buffer] buffer-string :face bold "\n" 1 [buffer-name kill-buffer] "Unknown author" time-to-seconds " • " telega-ins--date-full shadow [buffer-name kill-buffer] telega-link :max :elide t] 19 (#$ . 13111)])
#@52 Insert PageBlock PB for the instant view.

(fn PB)
(defalias 'telega-webpage--ins-pb #[257 "\303\304\305\"!\306\307\"\2038 \310\311\312\313!r\211q\210\314\315\316\317\320!\321\"\322$\216\323\304\324\"!\210\325 *\262\326\327D\"!\210\202\232\306\330\"\203I \323\304\331\"!\210\202\232\306\332\"\203\232 \310\311\312\313!r\211q\210\314\315\316\317\320!\333\"\322$\216\310\334!\210\323\304\335\"!\210\310\336!\210\304\337\"\211\315U\203\203 \340 \262\341!\266\325 *\262\326\342D\"!\210\310\343!\210\202\232\306\344\"\203\320 \310\311\312\313!r\211q\210\314\315\316\317\320!\345\"\322$\216\323\304\346\"!\210\310\343!\210\325 *\262\326\347D\"!\210\202\232\306\350\"\203\310\311\312\313!r\211q\210\314\315\316\317\320!\351\"\322$\216\323\304\352\"!\210\310\343!\210\325 *\262\326\353D\"!\210\202\232\306\354\"\203\323\304\355\"!\210\310\343!\210\202\232\306\356\"\203Q\310\311\312\313!r\211q\210\314\315\316\317\320!\357\"\322$\216\323\304\355\"!\210\310\343!\210\325 *\262\326\360D\"!\210\202\232\306\361\"\203\215\310\311\312\313!r\211q\210\314\315\316\317\320!\362\"\322$\216\310\363\322\245\364\"\343\"\210\323\304\365\"!\210\325 *\262\326\342D\"!\210\202\232\306\366\"\203\276\310\311\312\313!r\211q\210\314\315\316\317\320!\367\"\322$\216\310\363\370\"!\210\325 *\262\326\371D\"!\210\202\232\306\372\"\203\317\373\304\374\"!\210\202\232\306\375\"\203G\310\376\304\377\"\376Q!\210\201@ \201A  \203\361W\203\364\211\262\310\363Z\370\"!\210\310\311\312\313!r\211q\210\314\315\316\317\320!\201B \"\322$\216\210\201C \201D \304	\201E \"\"\210\325 *\262\201F \201G \201H \363\370\"\201I \201@ \257\"!\266\202\232\306\201J \"\203f\201K \201C \201D \304\201L \"\"\210)\202\232\306\201M \"\203\276\201N \n\201O \201P #\310!\210\310\311\312\313!r\211q\210\314\315\316\317\320!\201Q \"\322$\216\323\304\355\"!\210\325 *\262\201F \201G \201I \201H \257\"!\210\310\343!\266\202\232\306\201R \"\203\310\201S !\210\310\311\312\313!r\211q\210\314\315\316\317\320!\201T \"\322$\216\323\304\355\"!\210\325 *\262\201F \201U \201I \322Z\201H \201S \257\"!\210\310\343!\210\202\232\306\201V \"\203!\310\201W !\210\202\232\306\201X \"\2033\310\201Y !\210\202\232\306\201Z \"\203n\201[ \201\\ \304\201] \"\201^ \201_ \201` \201a &\210\310\343!\210\201K \201D \304\201b \"!\210)\202\232\306\201c \"\203\200\310\201d !\210\202\232\306\201e \"\203\227\201D \304\201f \"!\210\202\232\306\201g \"\203\271\201[ \201\\ \201^ \201h \201` \201i &\210\202\232\306\201j \"\203\313\310\201k !\210\202\232\306\201l \"\203\360\201C \201D \304\201E \"\"\210\201D \304\201b \"!\210\202\232\306\201m \"\203\310\201n !\210\202\232\306\201o \"\203d\310\311\312\313!r\211q\210\314\315\316\317\320!\201p \"\322$\216\310\201q \324\"\376\201r \201q 	\201s \"\376%\210\201t \201u \201v \201q \201s \"\201` \201w %\210\325 *\262\326\201x D\"!\210\202\232\306\201y \"\203\263\310\311\312\313!r\211q\210\314\315\316\317\320!\201z \"\322$\216\323\304\355\"!\210`\323\304\201{ \"!\203\244\212\211b\210\310\201| !\210)\210\325 *\262\326\342D\"!\210\202\232\306\201} \"\203\305\201~ !\210\202\232\306\201 \"\203\351\201K \323\304\201b \"!\210)\310\343!\210\310\201\200 !\210\202\232\306\201\201 \"\203-\201[ \201\\ \201^ \201\202 \201` \314\201\203 \201\204 \317\320\f!\201\205 \"\201\206 \201\207 %\201\210 \201\211 \304\201\212 \"P&\210\202\232\306\201\213 \"\203y\310\311\312\313!r\211q\210\314\315\316\317\320!\201\214 \"\322$\216\323\304\346\"!\203[\310\343!\210\325 *\262\326\201\215 D\"!\210\201C \201D \304\201\216 \"\"\210\202\232\306\201\217 \"\203\216\323\304\201\220 \"!\210\202\232\201\221 \201\222 \201\223 #\210\210	\204\260\303\304\305\"!\201\224 >\204\260\310\343!\210\201K \207" [telega-webpage-fill-column telega-webpage-strip-nl telega-symbol-vertical-bar intern plist-get :@type eql pageBlockTitle telega-ins telega-fmt-eval-attrs generate-new-buffer #1=" *temp*" make-byte-code 0 "\301\300!\205	 \302\300!\207" vconcat vector [buffer-name kill-buffer] 2 telega-webpage--ins-rt :title buffer-string :face telega-webpage-title pageBlockSubtitle :subtitle pageBlockAuthorDate [buffer-name kill-buffer] "By " :author " • " :publish_date time-to-seconds telega-ins--date-full shadow "\n" pageBlockHeader [buffer-name kill-buffer] :header telega-webpage-header pageBlockSubheader [buffer-name kill-buffer] :subheader telega-webpage-subheader pageBlockParagraph :text pageBlockPreformatted [buffer-name kill-buffer] telega-webpage-preformatted pageBlockFooter [buffer-name kill-buffer] make-string 45 :footer pageBlockDivider [buffer-name kill-buffer] 32 telega-webpage-strike-through pageBlockAnchor telega-webpage--add-anchor :name pageBlockListItem " " :label nil telega-current-column [buffer-name kill-buffer] mapc telega-webpage--ins-pb :page_blocks :fill left :fill-prefix :fill-column pageBlockList t :items pageBlockBlockQuote propertize face bold [buffer-name kill-buffer] pageBlockPullQuote "  " [buffer-name kill-buffer] center pageBlockAnimation "<TODO: pageBlockAnimation>\n" pageBlockAudio "<TODO: pageBlockAudio>\n" pageBlockPhoto telega-button--insert telega :photo :inserter #[257 "\301\302#\207" [telega-webpage-photo-maxsize telega-ins--photo nil] 5 "\n\n(fn PHOTO)"] :action telega-photo--open :caption pageBlockVideo "<TODO: pageBlockVideo>\n" pageBlockCover :cover pageBlockEmbedded #[257 "\301\302\"\301\303\"\301\304\"\305\306\307\310!r\211q\210\311\312\313\314\315!\316\"\317$\216\305!\210\320 *\262\321\322D\"!\2037 \305\323!\210\305\306\307\310!r\211q\210\311\312\313\314\315!\324\"\317$\216\305!\210\320 *\262\321\325D\"!\203a \305\323!\210\211\203k \326\327#\210`\330\301\331\"!\205~ \212\211b\210\305\323!)\262\207" [telega-webpage-photo-maxsize plist-get :url :html :poster_photo telega-ins telega-fmt-eval-attrs generate-new-buffer #1# make-byte-code 0 "\301\300!\205	 \302\300!\207" vconcat vector [buffer-name kill-buffer] 2 buffer-string :face (bold telega-link) "\n" [buffer-name kill-buffer] telega-webpage-fixed telega-ins--photo nil telega-webpage--ins-pb :caption] 13 "\n\n(fn PB-EMBEDDED)"] #[257 "\300\301\"\211\211\302\230\262?\205 \303!\207" [plist-get :url "" telega-browse-url] 5 "\n\n(fn PB-EMBEDDED)"] pageBlockEmbeddedPost "<TODO: pageBlockEmbeddedPost>\n" pageBlockCollage pageBlockSlideshow "<TODO: pageBlockSlideshow>\n" pageBlockChatLink [buffer-name kill-buffer] telega-tl-str "@" :username telega-ins--button "Open" :value telega-tme-open-username telega-webpage-chat-link pageBlockCaption [buffer-name kill-buffer] :credit " --" pageBlockDetails telega-webpage--ins-pb-details pageBlockTable "<TODO: pageBlockTable>\n" pageBlockRelatedArticle telega-webpage--ins-related-article 257 "\301\302\300\303\"!\207" [telega-browse-url plist-get :url] 5 "\n\n(fn PBIGNORED)" :help-echo "URL: " :url pageBlockRelatedArticles [buffer-name kill-buffer] (telega-msg-heading bold) :articles pageBlockKicker :kicker error "cl-ecase failed: %s, %s" (pageBlockTitle pageBlockSubtitle pageBlockAuthorDate pageBlockHeader pageBlockSubheader pageBlockParagraph pageBlockPreformatted pageBlockFooter pageBlockDivider pageBlockAnchor pageBlockListItem pageBlockList pageBlockBlockQuote pageBlockPullQuote pageBlockAnimation pageBlockAudio pageBlockPhoto pageBlockVideo pageBlockCover pageBlockEmbedded pageBlockEmbeddedPost pageBlockCollage pageBlockSlideshow pageBlockChatLink pageBlockCaption pageBlockDetails pageBlockTable pageBlockRelatedArticle pageBlockRelatedArticles pageBlockKicker) (pageBlockAnchor pageBlockCover pageBlockListItem pageBlockDetails)] 14 (#$ . 14436)])
#@88 Same as `telega-webpage--ins-PageBlock', but also inserts newline at the end.

(fn PB)
(defalias 'telega-webpage--ins-pb-nl #[257 "\300!\205	 \301\302!\207" [telega-webpage--ins-pb telega-ins "\n"] 3 (#$ . 22148)])
#@162 Instantly view webpage by URL.
If INSTANT-VIEW is non-nil, then its value is already fetched
instant view for the URL.

(fn URL &optional SITENAME INSTANT-VIEW)
(defalias 'telega-webpage--instant-view #[769 "\306\307\310!!\210	GW\203 	8`\240\210\2060 \311\312!\313!\n>\204* \314\315\316D\"\210\211\317H\262!\211\206? \320!\206? \321\322\"\323\f\324\"\206H \3250\326 \210\325p12\327 \210\330\331\323\f\332\"\"\2103\203l \333\334\335\f\"!\210eb\210*4\336=\204\212 \336 \210\337\340!\210\341\340!\21056\342\340!\210\343\344\345\206\223 \346\347Q\"\203\245 \350\351\352\211$\266\202\202\250 \266\202\211\211\351\230\262\204\265 \353!\210\210\354\355\356\357!\"\207" [telega-webpage-history--index telega-webpage-history cl-struct-url-tags telega-webpage--sitename telega-webpage--iv telega-webpage--url pop-to-buffer-same-window get-buffer-create "*Telega Instant View*" capitalize url-generic-parse-url type-of signal wrong-type-argument url 4 telega--getWebPageInstantView error "Can't instant view the URL: %s" plist-get :url nil telega-webpage--history-push erase-buffer mapc telega-webpage--ins-pb :page_blocks telega-ins format "\n---DEBUG---\n%S" telega-webpage-mode cursor-sensor-mode 1 visual-line-mode visual-fill-column-mode "[^#]*#?" string-match "\\`\\(?:" "[ 	\n]+" "\\)" replace-match "" t telega-webpage-goto-anchor message "Press `%s' to open in web browser" substitute-command-keys "\\[telega-webpage-browse-url]" telega-webpage--anchors telega--current-buffer buffer-read-only telega-debug major-mode telega-webpage-fill-column visual-fill-column-width] 10 (#$ . 22371)])
#@76 Return non-nil if URL is pointing to anchor for current webpage.

(fn URL)
(defalias 'telega-webpage-anchor-url-p #[257 "\301\302\303\304\206\n \305\306Q\"\203 \307\310\311\211$\266\202\202 \266\202\312P\"\207" [telega-webpage--url string-prefix-p "#.*" string-match "\\(?:" "[ 	\n]+" "\\)\\'" replace-match "" t "#"] 9 (#$ . 23993)])
#@236 Open the URL.
If URL can be opened directly inside telega, then do it.
Invite links and link to users can be directly opened in telega.
If IN-WEB-BROWSER is non-nil then force opening in web browser.

(fn URL &optional IN-WEB-BROWSER)
(defalias 'telega-browse-url #[513 "\211\204\207 \300\301\"\203 \302!\202\203 \300\303\"\204- \300\304\"\204- \300\305\"\204- \300\306\"\2033 \307!\202\203 \300\310\"\203B \307\311P!\202\203 \312\313!\203s \314!\203s \315\316\317\320\206X \321\322Q\"\203j \323\324\325\211$\266\202\202m \266\202!\210\325\202\203 \326!\211\205\201 \327\330#\210\325\262?\205\212 \331!\207" [string-prefix-p "tg:" telega-tme-open-tg "https://t.me/" "http://t.me/" "https://telegram.me/" "https://telegram.dog/" telega-tme-open "t.me/" "https://" derived-mode-p telega-webpage-mode telega-webpage-anchor-url-p telega-webpage-goto-anchor "[^#]+#" string-match "\\`\\(?:" "[ 	\n]+" "\\)" replace-match "" t telega--getWebPageInstantView telega-webpage--instant-view "Telegra.ph" browse-url] 10 (#$ . 24344)])
(provide 'telega-webpage)

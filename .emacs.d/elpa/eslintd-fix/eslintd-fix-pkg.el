;;; -*- no-byte-compile: t -*-
(define-package "eslintd-fix" "20190830.2116" "use eslint_d to automatically fix js files" '((dash "2.12.0") (emacs "24.3")) :commit "98c669e3653bf94c236c54946c6faba7f782ef0d" :authors '(("Aaron Jensen" . "aaronjensen@gmail.com")) :maintainer '("Aaron Jensen" . "aaronjensen@gmail.com") :url "https://github.com/aaronjensen/eslintd-fix")

;;; -*- no-byte-compile: t -*-
(define-package "magit-libgit" "20190419.1545" "Libgit functionality" '((emacs "26.1") (magit "0") (libgit "0")) :commit "d1d8e49885bd026d888f3ecb4eb92179dca480a4" :keywords '("git" "tools" "vc") :authors '(("Jonas Bernoulli" . "jonas@bernoul.li")) :maintainer '("Jonas Bernoulli" . "jonas@bernoul.li") :url "https://github.com/magit/magit")

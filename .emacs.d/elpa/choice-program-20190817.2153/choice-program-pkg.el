(define-package "choice-program" "20190817.2153" "parameter based program"
  '((emacs "26"))
  :keywords
  '("exec" "execution" "parameter" "option")
  :authors
  '(("Paul Landes"))
  :maintainer
  '("Paul Landes")
  :url "https://github.com/plandes/choice-program")
;; Local Variables:
;; no-byte-compile: t
;; End:

;;; gtk-variant-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "gtk-variant" "gtk-variant.el" (0 0 0 0))
;;; Generated autoloads from gtk-variant.el

(autoload 'gtk-variant-set-frame "gtk-variant" "\
Set the GTK theme variant of frame FRAME to VARIANT.
With no arguments, sets the selected frame to the variable `gtk-variant'

Recommended usage:
\(add-hook 'window-setup-hook #'gtk-variant-set-frame)
\(add-hook 'after-make-frame-functions #'gtk-variant-set-frame)

\(fn &optional FRAME VARIANT)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "gtk-variant" '("gtk-variant")))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; gtk-variant-autoloads.el ends here

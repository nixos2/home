(define-package "buffer-manage" "20190815.502" "manage buffers"
  '((emacs "26")
    (choice-program "0.5")
    (dash "2.13.0"))
  :keywords
  '("interactive" "buffer" "management")
  :authors
  '(("Paul Landes"))
  :maintainer
  '("Paul Landes")
  :url "https://github.com/plandes/buffer-manage")
;; Local Variables:
;; no-byte-compile: t
;; End:

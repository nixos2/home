;;; Generated package description from /home/apterion/.emacs.d/elpa/mlso-theme-20200513.1751/mlso-theme.el  -*- no-byte-compile: t -*-
(define-package "mlso-theme" "20200513.1751" "A dark, medium contrast theme" '((emacs "24")) :commit "059c5773403b73a14a1b04d62d1d5b077a3f6e06" :authors '(("github.com/Mulling")) :maintainer '("github.com/Mulling") :url "https://github.com/Mulling/mlso-theme")

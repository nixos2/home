(define-package "color-theme-modern" "20200315.929" "Reimplement colortheme with Emacs 24 theme framework."
  '((emacs "24"))
  :url "https://github.com/emacs-jp/replace-colorthemes/")
;; Local Variables:
;; no-byte-compile: t
;; End:

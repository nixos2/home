;;; -*- no-byte-compile: t -*-
(define-package "auto-complete-distel" "20160815.2300" "Erlang/distel completion backend for auto-complete-mode" '((auto-complete "1.4") (distel-completion-lib "1.0.0")) :commit "2ba4eea51cecfa75cf62f58180460ee9ff43131f" :url "github.com/sebastiw/distel-completion" :keywords '("erlang" "distel" "auto-complete"))

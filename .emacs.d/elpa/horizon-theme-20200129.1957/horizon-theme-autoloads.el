;;; horizon-theme-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "horizon-theme" "horizon-theme.el" (0 0 0 0))
;;; Generated autoloads from horizon-theme.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "horizon-theme" '("horizon")))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; horizon-theme-autoloads.el ends here

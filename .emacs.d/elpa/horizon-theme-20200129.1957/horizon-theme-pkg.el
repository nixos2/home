;;; Generated package description from /home/apterion/.emacs.d/elpa/horizon-theme-20200129.1957/horizon-theme.el  -*- no-byte-compile: t -*-
(define-package "horizon-theme" "20200129.1957" "A beautifully warm dual theme" '((emacs "24.3")) :commit "040d19abd397d2132508a50e1266e86d324f7c69" :url "https://github.com/aodhneine/horizon-theme.el")

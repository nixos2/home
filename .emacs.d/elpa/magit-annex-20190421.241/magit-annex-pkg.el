;;; -*- no-byte-compile: t -*-
(define-package "magit-annex" "20190421.241" "Control git-annex from Magit" '((cl-lib "0.3") (magit "2.90.0")) :commit "d5d819c609256a3b7b11ccaf6664be61aa3597b6" :keywords '("vc" "tools") :authors '(("Kyle Meyer" . "kyle@kyleam.com") ("Rémi Vanicat" . "vanicat@debian.org")) :maintainer '("Kyle Meyer" . "kyle@kyleam.com") :url "https://github.com/magit/magit-annex")

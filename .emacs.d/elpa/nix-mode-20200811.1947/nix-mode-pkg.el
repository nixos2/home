(define-package "nix-mode" "20200811.1947" "Major mode for editing .nix files"
  '((emacs "25"))
  :commit "0cf1ea1e0ed330b59f47056d927797e625ba8f53" :keywords
  '("nix" "languages" "tools" "unix")
  :maintainer
  '("Matthew Bauer" . "mjbauer95@gmail.com")
  :url "https://github.com/NixOS/nix-mode")
;; Local Variables:
;; no-byte-compile: t
;; End:

(define-package "libgit" "20191217.1254" "Thin bindings to libgit2."
  '((emacs "25.1"))
  :keywords
  '("git" "vc")
  :authors
  '(("Eivind Fonn" . "evfonn@gmail.com"))
  :maintainer
  '("Eivind Fonn" . "evfonn@gmail.com")
  :url "https://github.com/TheBB/libegit2")
;; Local Variables:
;; no-byte-compile: t
;; End:

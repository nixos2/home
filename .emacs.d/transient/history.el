((magit-branch nil)
 (magit-commit nil)
 (magit-diff
  ("--no-ext-diff")
  ("--no-ext-diff" "--stat"))
 (magit-dispatch nil)
 (magit-ediff nil)
 (magit-fetch nil
              ("--prune"))
 (magit-gitignore nil)
 (magit-log
  ("-n256" "--graph" "--decorate"))
 (magit-pull nil)
 (magit-push nil
             ("--force-with-lease"))
 (magit-rebase nil
               ("--interactive"))
 (magit-stash nil))


(add-hook 'realgud-short-key-mode-hook
          (lambda ()
            (local-set-key "\C-c" realgud:shortkey-mode-map)))

(global-set-key (kbd "C-c h e") #'hasky-stack-execute)
(global-set-key (kbd "C-c h h") #'hasky-stack-package-action)
(global-set-key (kbd "C-c h i") #'hasky-stack-new)
(global-set-key (kbd "C-c h e") #'hasky-stack-execute)
(global-set-key (kbd "C-c h h") #'hasky-stack-package-action)
(global-set-key (kbd "C-c h i") #'hasky-stack-new)


;; Magit
(global-set-key (kbd "C-c p v") 'magit)

;; Resize panes
(global-set-key (kbd "<C-up>") 'shrink-window)
(global-set-key (kbd "<C-down>") 'enlarge-window)
(global-set-key (kbd "<C-left>") 'shrink-window-horizontally)
(global-set-key (kbd "<C-right>") 'enlarge-window-horizontally)

;; Grep
(global-set-key (kbd "C-c g") 'grep-find)
